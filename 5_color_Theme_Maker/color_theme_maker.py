#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH  date 19 feb 2024


import tkinter
from tkinter import BOTH,IntVar,DISABLED,filedialog

#main window 
root=tkinter.Tk()
root.title('Garcha Color Theme Maker')
root.iconbitmap('color_wheel.ico')
root.geometry('450x500')
root.resizable(0,0)

#font and color 
#define functions 
def get_col(event):
        global final_hex_color
        red=r_slider.get()
        blue=b_slider.get()
        green=g_slider.get()
        
        r_hex= hex(red).lstrip("0x")                # to remove 0x infront of hex example 0xff = ff 
        g_hex= hex(green).lstrip("0x")
        b_hex= hex(blue).lstrip("0x")
        
        while len(r_hex)<2:                         # to add 0 infront of hex example 0x4 = 04 otherwise it will 4 which create problem
            r_hex="0"+str(r_hex)
        while len(g_hex)<2:
            g_hex="0"+str(g_hex)
        while len(b_hex)<2:
            b_hex="0"+str(b_hex)
        
        final_hex_color = "#"+r_hex+g_hex+b_hex
        color_box_2=tkinter.Label(input_frame,bg=final_hex_color,width=15,height=5)  
        color_box_2.grid(row=1,column=3,columnspan=2,padx=35,pady=10)
        color_hex.config(text=final_hex_color)
        color_tuple.config(text='(' + str(red) + '),('+ str(green) +'),(' + str(blue) + ')')
        print((red),(green),(blue))
        print(final_hex_color)
        
def set_color(r,g,b):
    r_slider.set(r)
    g_slider.set(g)
    b_slider.set(b)       


def store_color():
     get_col(1)
     global stored_color_dictonary

     red = str(r_slider.get())
     while len(red)<3:
        red = "0"+ red 

     green = str(g_slider.get())
     while len(green)<3:
        green = "0"+ green 

     blue = str(b_slider.get())
     while len(blue)<3:
        blue = "0"+ blue 

     stored_red   =r_slider.get()
     stored_green =g_slider.get()
     stored_blue  =b_slider.get()

     recall_button= tkinter.Button(output_frame,text='Recall Color',command=lambda:set_color(stored_red ,stored_green ,stored_blue ))
     recall_button.grid(row=stored_color.get(),column=1,padx=20)
     
     output_int=tkinter.Label(output_frame,text='(' + red + '),('+ green +'),(' + blue + ')')
     output_int.grid(row=stored_color.get(),column=2,padx=20)
     
     output_hex=tkinter.Label(output_frame,text=final_hex_color)
     output_hex.grid(row=stored_color.get(),column=3,padx=20)

     output_black_box=tkinter.Label(output_frame,bg='black',width=3,height=1)
     output_white_box=tkinter.Label(output_frame,bg=final_hex_color,width=3,height=1)
     output_black_box.grid(row=stored_color.get(),column=4,pady=2,ipadx=5,ipady=5)
     output_white_box.grid(row=stored_color.get(),column=4,pady=2)


     #update dict with hex and int value 
     stored_color_dictonary[stored_color.get()] =[output_int.cget('text'),output_hex.cget('text')] 

        #this is for moving radio button to next one 
     if stored_color.get() < 5:
            stored_color.set(stored_color.get()+1)

#def save 
def save():
    print(stored_color_dictonary)
    location = filedialog.asksaveasfilename(
                                            initialdir='./',
                                            title = ('Save Colours'),
                                            filetype = (('text files','*.txt'),('All files','*.*'))

                                           ) 
    with open (location,'w')as my_file:
        my_file.write("color Theme Maker Output\n")
        for items in stored_color_dictonary.values():
            my_file.write(items[0]+'\n'+items[1]+'\n')

#define layout 
input_frame = tkinter.LabelFrame(root,padx=5,pady=5)
output_frame=tkinter.LabelFrame(root,padx=5,pady=5)
input_frame.pack(padx=5,pady=5,fill=BOTH,expand=True)
output_frame.pack(padx=5,pady=5,fill=BOTH,expand=True) 


#input widgets 
#----------------------------------------------------------------------input widgets------------------------------------------------------------#
#r g b label
r_label=tkinter.Label(input_frame,text="R")
g_label=tkinter.Label(input_frame,text='G')
b_label=tkinter.Label(input_frame,text='B')

#r r b slider 
r_slider=tkinter.Scale(input_frame,from_=0,to=255,command=get_col)
g_slider=tkinter.Scale(input_frame,from_=0,to=255,command=get_col)
b_slider=tkinter.Scale(input_frame,from_=0,to=255,command=get_col)

#r g b button 
r_button=tkinter.Button(input_frame,text='Red',command=lambda:set_color(255,0,0))
g_button=tkinter.Button(input_frame,text='Green',command=lambda:set_color(0,255,0))
b_button=tkinter.Button(input_frame,text='Blue',command=lambda:set_color(0,0,255))

#y c m button
y_button=tkinter.Button(input_frame,text='Yellow',command=lambda:set_color(255,255,0))
c_button=tkinter.Button(input_frame,text='Cyan',command=lambda:set_color(0,255,255))
m_button=tkinter.Button(input_frame,text='Magenta',command=lambda:set_color(255,0,255))

#store save quit button 
st_button=tkinter.Button(input_frame,text='Store color',command=store_color)
sv_button=tkinter.Button(input_frame,text='Save',command=save)
q_button=tkinter.Button(input_frame,text='Quit',command=root.destroy)

#colour box label, colour label , colour hex label
color_box=tkinter.Label(input_frame,bg='black',width=15,height=5,borderwidth=2)
color_tuple=tkinter.Label(input_frame,text='(0),(0),(0)')
color_hex=tkinter.Label(input_frame,text='#000000')


#grid system
r_label.grid(row=0,column=0,sticky="W")
g_label.grid(row=0,column=1,sticky="W")
b_label.grid(row=0,column=2,sticky="W")

r_slider.grid(row=1,column=0,sticky="W")
g_slider.grid(row=1,column=1,sticky="W")
b_slider.grid(row=1,column=2,sticky="W")
color_box.grid(row=1,column=3,columnspan=2,padx=35,pady=10,ipadx=10,ipady=10)

r_button.grid(row=2,column=0,padx=1,pady=1,ipadx=20,sticky="WE")
g_button.grid(row=2,column=1,padx=1,pady=1,ipadx=15,sticky="WE")
b_button.grid(row=2,column=2,padx=1,pady=1,ipadx=18,sticky="WE")
color_tuple.grid(row=2,column=3,columnspan=2)

y_button.grid(row=3,column=0,padx=1,pady=1,sticky="WE")
c_button.grid(row=3,column=1,padx=1,pady=1,sticky="WE")
m_button.grid(row=3,column=2,padx=1,pady=1,sticky="WE")
color_hex.grid(row=3,column=3,columnspan=2)

st_button.grid(row=4,column=0,columnspan=3,padx=1,pady=1,sticky="WE")
sv_button.grid(row=4,column=3,padx=1,pady=1,sticky="WE")
q_button.grid(row=4,column=4,padx=1,pady=1,sticky="WE")
#----------------------------------------------------------------------input widgets------------------------------------------------------------#
#----------------------------------------------------------------------output widgets------------------------------------------------------------#
stored_color_dictonary={}
stored_color=IntVar()

for i in range(6):
    radio_button = tkinter.Radiobutton(output_frame,variable=stored_color,value=i)
    recall_button = tkinter.Button(output_frame,text='Recall Color',state=DISABLED)
    output_int=tkinter.Label(output_frame,text='(255),(255),(255)')
    output_hex=tkinter.Label(output_frame,text='#ffffff')
    output_black_box=tkinter.Label(output_frame,bg='black',width=3,height=1)
    output_white_box=tkinter.Label(output_frame,bg='white',width=3,height=1)

    radio_button.grid(row=i,column=0,sticky='W')
    recall_button.grid(row=i,column=1,padx=20)
    output_int.grid(row=i,column=2,padx=20)
    output_hex.grid(row=i,column=3,padx=20)
    output_black_box.grid(row=i,column=4,pady=2,ipadx=5,ipady=5)
    output_white_box.grid(row=i,column=4,pady=2)
    
    #stored_color_dictonary[stored_color.get()]= [output_int.cget('text'),output_hex.cget('text')] 


#----------------------------------------------------------------------output widgets------------------------------------------------------------#
#main loop
root.mainloop()