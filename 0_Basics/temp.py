import tkinter
from tkinter import IntVar

#define window
root=tkinter.Tk()
root.title('Simran')
root.iconbitmap("thinking.ico")
root.geometry('400x400')
root.resizable(0,0)


#define function
def make_label():
    if (number.get() == 75 ):
        print_label=tkinter.Label(output_frame,text="simran wt is 75")
    
    elif (number.get() == 45):
        print_label=tkinter.Label(output_frame,text="simran wt is 45")

    print_label.pack()
        

#define frame
input_frame  = tkinter.LabelFrame(root,text='It is fun!',width = 400 , height=100)
output_frame = tkinter.Label(root,width = 400 , height=300)

input_frame.pack(padx=10,pady=10)
output_frame.pack(padx=10,pady=(0,10))

#define var
number = IntVar()

#define radio button
radio_button_1 = tkinter.Radiobutton(input_frame,text="Simran wg is 75kg ",variable= number, value=75)
radio_button_2 = tkinter.Radiobutton(input_frame,text="Simran wg is 45kg ",variable= number, value=45)
print_button   = tkinter.Button(input_frame,text="print Simran wg",command=make_label)

radio_button_1.grid(row=0 ,column=0,padx=10,pady=10)
radio_button_2.grid(row=0,column=1,padx=10,pady=10)
print_button.grid(row =1 , column=0,columnspan=2,padx=10,pady=10)
#my app
root.mainloop()

