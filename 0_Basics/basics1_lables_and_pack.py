#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 
#

#lables and pack

import tkinter


#define window 

root = tkinter.Tk()
root.title('Label basics')
root.iconbitmap('thinking.ico')
root.geometry('400x500')
root.resizable(0,0)
root.config(bg="green")

#Create widegets

#name_lable_1 = tkinter.Label(root, text='Hello , my name is Mike.')
#name_lable_1.pack()

name_label_2 = tkinter.Label(root, text='Hello, my name is John', font=('Arial', 18, 'bold'))
name_label_2.pack()

name_label_3 = tkinter.Label(root)
name_label_3.config(text='Hello, my name is Paul')
name_label_3.config(font=('Cambria',10))
name_label_3.config(bg="red")
name_label_3.pack(padx=1, pady=80)

name_label_4 = tkinter.Label(root, text="hi may name is ashwinder",bg='yellow',fg='purple')
name_label_4.pack(pady=(0,10),ipadx=50,ipady=10,anchor = 'w')

name_lable_5=tkinter.Label(root,text="hi all",font=('arial',18,'bold'),bg='white',fg='black')
name_lable_5.pack(fill='both',expand=True,padx=50,pady=50)
#run the root window's main
root.mainloop()