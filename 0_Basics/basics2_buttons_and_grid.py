#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 
#

#Buttons and Grid

import tkinter

#define window
root=tkinter.Tk()
root.title("Buttons and grid")
root.iconbitmap('thinking.ico')
root.geometry('400x400')
root.resizable(0,0)


#define layout
#button_1=tkinter.Button(root,text="name")
#button_1.grid(row=0,column=0)

time_button=tkinter.Button(root,text='time',bg="#00ffff")
time_button.grid(row=0,column=1)

place_button= tkinter.Button(root,text='place',bg='#00ffff',activebackground='#ff0000')
place_button.grid(row=0,column=2,padx=10,pady=10,ipadx=15,ipady=15)

day_button=tkinter.Button(root,text='day',bg='gray',borderwidth=5)
day_button.grid(row=1,column=0,columnspan=3,sticky='we')

test1=tkinter.Button(root,text='test',)
test2=tkinter.Button(root,text='test',)
test3=tkinter.Button(root,text='test',)
test4=tkinter.Button(root,text='test',)
test5=tkinter.Button(root,text='test',)
test6=tkinter.Button(root,text='test',)

test1.grid(row=2,column=0,padx=5,pady=5)
test2.grid(row=2,column=1,padx=5,pady=5)
test3.grid(row=2,column=2,padx=5,pady=5,sticky="w")
test4.grid(row=3,column=0,padx=5,pady=5)
test5.grid(row=3,column=1,padx=5,pady=5)
test6.grid(row=3,column=2,padx=5,pady=5,sticky='W')

#call root window's main loop
root.mainloop()