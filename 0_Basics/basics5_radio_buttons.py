#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 
#Radio buttons

import tkinter
from tkinter import IntVar
#define window 
root=tkinter.Tk()
root.title('Radio buttons')
root.iconbitmap('thinking.ico')
root.geometry("400x400")
root.resizable(0,0)

#define function
def make_label():
    
    if number.get() == 1:
        text=tkinter.Label(output_frame,text='1 on 1')
       
    elif number.get() == 2:
        text=tkinter.Label(output_frame,text='2 on 2')
    
    text.pack()

#define Frames
input_frame = tkinter.LabelFrame(root,text="This is fun!",width=400,height=100)
output_frame =tkinter.Label(root,width=400,height=300)

input_frame.pack(padx=10,pady=10)
output_frame.pack(padx=10,pady=(0,10))

#define Widgets
number=IntVar()
#number.set(1)
radio_button_1 =tkinter.Radiobutton(input_frame,text='Print the number one!',variable=number,value=1)
radio_button_2 =tkinter.Radiobutton(input_frame,text='Print the number Two!',variable=number,value=2)
print_button =tkinter.Button(input_frame,text='Print the number',command=make_label)


radio_button_1.grid(row=0,column=0,padx=10,pady=10)
radio_button_2.grid(row=0,column=1,padx=10,pady=10)
print_button.grid(row=1,column=0,columnspan=2,padx=10,pady=10)
#my app
root.mainloop()