#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 
#

import tkinter
from tkinter import BOTH
#define window
root = tkinter.Tk()
root.title("FRAMES")
root.iconbitmap("thinking.ico")
root.geometry("400x400")
root.resizable(0,0)


#why to use frames 
#name_label_1=tkinter.Label(root,text='Enter your Name')
#name_label_1.pack()
#name_button_1=tkinter.Button(root,text='Submit your name')
#name_button_1.grid(row=0,column=0)

#define frames 
pack_frame=tkinter.Frame(root,bg='red')
grid_frame_1=tkinter.Frame(root,bg='blue')
grid_frame_2=tkinter.LabelFrame(root,text='Grid system rules',borderwidth=5)

#Pack frames onto root
pack_frame.pack(fill=BOTH,expand=True)
grid_frame_1.pack(fill=BOTH,expand=True)
grid_frame_2.pack(fill=BOTH,expand=True,padx=10,pady=10)


#pack frame 
lable_1=tkinter.Label(pack_frame,text="Enter your name")
lable_1.pack()

#grid layout
tkinter.Button(grid_frame_1,text='Submit you name').grid(row =0 ,column =5 )
#grid layout 2 
tkinter.Button(grid_frame_2,text="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa").grid(row=3,column=0)
#main window
root.mainloop()