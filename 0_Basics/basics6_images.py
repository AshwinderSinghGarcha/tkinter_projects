#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 

import tkinter
from PIL import ImageTk , Image
#define window 
root=tkinter.Tk()
root.title("Image basics!")
root.iconbitmap("Aha-Soft-Transport-For-Vista-Rocket.ico")
root.geometry('512x512')
#root.resizable(0,0)

#define function
def make_image():
    global cat_image   #importance of global it will remove image if we dont use global
    #using PIL for JPG
    cat_image =ImageTk.PhotoImage(Image.open("cat.jpg"))
    cat_label=tkinter.Label(root,image=cat_image)
    cat_label.pack()


#basics .....Works for png
my_img = tkinter.PhotoImage(file="Aha-Soft-Transport-For-Vista-Rocket.256.PNG")
my_label=tkinter.Label(root,image=my_img)
my_label.pack()
my_button=tkinter.Button(root,image=my_img,borderwidth=5)
my_button.pack()

#not for jpeg

#cat_image= tkinter.PhotoImage(file="cat.jpg")
#cat_label=tkinter.Label(root,image=cat_image)
#cat_label.pack()



make_image()
#my app
root.mainloop()