#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 
#

#Entries and Functions
import tkinter
from tkinter import BOTH
from tkinter import END



#define window 
root = tkinter.Tk()
root.title("Entry Basics!")
root.iconbitmap("thinking.ico")
root.geometry("400x400")
root.resizable(0,0)


#define function 
def make_label():
    #Print a label to the app
    text=tkinter.Label(output_frame,text=text_entry.get(),bg='red')
    text.pack()
    
    text_entry.delete(0,END)


def count_up(number):
    global value

    counter=tkinter.Label(output_frame,text=number,bg='red')
    counter.pack()

    value = number + 1


#define frames 
input_frame = tkinter.Frame(root,bg='#0000ff',width = 400,height=100)
output_frame = tkinter.Frame(root,bg='#ff0000',width = 400,height=300)

input_frame.pack(padx=10,pady=10)
output_frame.pack(padx=10,pady=(0,10))
output_frame.pack_propagate(0)

#entry widget
text_entry=tkinter.Entry(input_frame,width=40)
text_entry.grid(row=0,column=0,pady=5,padx=5)
input_frame.grid_propagate(0)


print_button=tkinter.Button(input_frame,text='Print',borderwidth=5,command=make_label)
print_button.grid(row=0,column=1,padx=5,pady=5,ipadx=30)



#pass a paramter with lambda
value = 0
count_button= tkinter.Button(input_frame,text='Count',command=lambda :count_up(value))
count_button.grid(row=1,column=0,columnspan=2,padx=5,pady=5,sticky="WE")

#run the root window main loop
root.mainloop()