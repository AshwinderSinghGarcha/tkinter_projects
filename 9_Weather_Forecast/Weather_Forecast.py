#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH  date 9 March 2024
# weather api https://openweathermap.org/api

import tkinter ,requests
from tkinter import BOTH ,IntVar 
from PIL import ImageTk,Image
from io import BytesIO

#define window
root= tkinter.Tk()
root.title('Garcha Weather Forecast')
root.iconbitmap('weather.ico')
root.geometry('400x400')
root.resizable(0,0)

#define font and colours 
sky_color='#76c3ef'
grass_color='#aad207'
output_color='#dcf0fb'
input_color='#ecf2ae'
large_font=('Simsun',14)
small_font=('Simsun',10)


#define search
def search():
    global response
    # call api by city name "https://api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}"
    url   = 'https://api.openweathermap.org/data/2.5/weather'
    api_key = 'b3e830cd68a7fb7c6710f01688256963'
    # api call to change temp units 
    # https://api.openweathermap.org/data/2.5/weather?lat=57&lon=-2.15&appid={API key}&units=metric
    #search by city or zip 
    if search.get() == 1 :
        #search by city 
        querystring={"q"    :search_entry.get(),
                     "appid":api_key,
                     "units":'metric'
                    }
    elif search.get() == 0:
        #search by zip
        querystring={"q"    :search_entry.get(), 
                     "appid":api_key,
                     "units":'metric'
                    }

    response = requests.request("GET",url,params=querystring)
    #print(response)
    response=response.json()
    #print(response)
    
    # below is example response of city kharar in json format 
    '''{'coord': {'lon': 76.6478, 'lat': 30.7444},
        'weather': [{'id': 800, 'main': 'Clear', 'description': 'clear sky', 'icon': '01n'}], 
        'base': 'stations', 
        'main': {'temp': 290.61, 'feels_like': 289.4, 'temp_min': 290.61, 'temp_max': 290.61, 'pressure': 1015, 'humidity': 38, 'sea_level': 1015, 'grnd_level': 978}, 
        'visibility': 10000, 
        'wind': {'speed': 3.24, 'deg': 7, 'gust': 4.1}, 
        'clouds': {'all': 0}, 
        'dt': 1709999104, 
        'sys': {'country': 'IN', 'sunrise': 1709946640, 'sunset': 1709989044}, 
        'timezone': 19800, 
        'id': 1266960, 
        'name': 'Kharar', 
        'cod': 200}'''
    get_icon()
    get_weather()
    #get_icon()


def get_weather():
    ''' take data from json response based on search and update output labels '''

    main_weather = response['weather'][0]['main']
    description  = response['weather'][0]['description']


    city_label.config(text= response['name'] +"(" + str(response['coord']['lat']) + "," + str(response['coord']['lon']) +")" ,font=large_font )
    weather_label.config(text="Weather: " + main_weather + "," + description  ,font=small_font )
    temp_label.config(text='Temprature: ' + str(response['main']['temp']) +" c",font=small_font )
    feels_label.config(text='Feels like: '+ str(response['main']['feels_like']) +" c",font=small_font )
    temp_min_label.config(text='Min Temprature: '+ str(response['main']['temp_min']) +" c",font=small_font )
    temp_max_label.config(text='Max Temprature: '+ str(response['main']['temp_max']) +" c",font=small_font )
    humidity_label.config(text='Humidity: '+ str(response['main']['humidity']),font=small_font )
    photo_label.config(image=img)
    
def get_icon():
    global img
    '''getting weather icon from api'''
    icon_id = str(response['weather'][0]['icon'])
    #print(icon_id)
    
    #get icon from wesbite
    url = 'https://openweathermap.org/img/wn/{icon}.png'.format(icon=icon_id) 
    
    icon_response=requests.get(url,stream=True)
    #print(icon_response)
    img_data=icon_response.content
    #print(img_data)
    covert_data_image = BytesIO(img_data)
    #print(covert_data_image)
    img = ImageTk.PhotoImage(Image.open(covert_data_image))
    #print(img)
#layout
sky_frame   = tkinter.Frame(root,bg=sky_color,height=250)
grass_frame = tkinter.Frame(root,bg=grass_color)
sky_frame.pack(fill=BOTH,expand=True)
grass_frame.pack(fill=BOTH,expand=True)

output_frame =tkinter.LabelFrame(sky_frame,bg=output_color,width=325,height=225)
input_frame  =tkinter.LabelFrame(grass_frame,bg=input_color,width=325)

output_frame.pack(pady=30)
output_frame.pack_propagate(0)
input_frame.pack(pady=10)


#output frame layout city ,temp,feels,temp min , temp max , hu
city_label=tkinter.Label(output_frame,bg=output_color)
weather_label=tkinter.Label(output_frame,bg=output_color)
temp_label=tkinter.Label(output_frame,bg=output_color)
feels_label=tkinter.Label(output_frame,bg=output_color)
temp_min_label=tkinter.Label(output_frame,bg=output_color)
temp_max_label=tkinter.Label(output_frame,bg=output_color)
humidity_label=tkinter.Label(output_frame,bg=output_color)
photo_label=tkinter.Label(output_frame,bg=output_color)

city_label.pack(pady=8)
weather_label.pack()
temp_label.pack()
feels_label.pack()
temp_min_label.pack()
temp_max_label.pack()
humidity_label.pack()
photo_label.pack(pady=8)


#input frame layout entry box , submit button , radio button 1,2 for search by city , or search by zip code

search_entry=tkinter.Entry(input_frame,width=20,bg='white',font=large_font)
submit_search=tkinter.Button(input_frame,text='Search',bg=input_color,font=large_font,command=search)

search = IntVar()
search.set(1)
search_city_radio=tkinter.Radiobutton(input_frame,text='Search by City name',font=small_font,bg=input_color,variable=search,value=1)
search_zip_radio=tkinter.Radiobutton(input_frame,text='Search by zipcode',font=small_font,bg=input_color,variable=search,value=0)

search_entry.grid(row=0,column=0,pady=(10,0),padx=10)
submit_search.grid(row=0,column=1,pady=(10,0),padx=10)

search_city_radio.grid(row=1,column=0,pady=2)
search_zip_radio.grid(row=1,column=1,padx=5,pady=2)
#define main loop 
root.mainloop()