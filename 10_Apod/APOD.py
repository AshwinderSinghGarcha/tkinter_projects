#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH  date 11 March 2024
# https://api.nasa.gov/
#key 

import tkinter 
from tkcalendar import DateEntry 
import requests,webbrowser
from PIL import ImageTk, Image
from io import BytesIO
from tkinter import messagebox ,filedialog

#main window 
root = tkinter.Tk()
root.title('Garcha APOD Photo Viewer ')
root.iconbitmap('rocket.ico')


#color and font 
text_font = ('Times New Roman',14)
nasa_blue='#043c93'
nasa_red='#ff1923'
nasa_light_blue='#7aa5d3'
nasa_white='#ffffff'
root.config(bg=nasa_blue)

#define submit 
def submit():
    global response
    date=calendar.get_date()
    print(date)
    my_KEY='jzl7cIF5J1mejsYPSQAWpfUELaBxlxYKWZULQScz'
    url = 'https://api.nasa.gov/planetary/apod'
    querystring = {'api_key':my_KEY,
                    'date' :date
                  }
    response = requests.request('GET',url,params=querystring)
    response = response.json()
    #print(response)
    get_info()
    


def get_info():
    
    global thumb
    global smallpic
    picture_date.config(text=response['date'])
    picture_explanation.config(text=response['explanation'])
    url = response['url']
    '''{
        'copyright': '\nSeung Hye Yang\n', 
        'date': '2024-03-12',
        'explanation': "What's that over the horizon? What may look like a strangely nearby galaxy is actually a normal 
        rocket's exhaust plume -- but unusually backlit.  Although the SpaceX Falcon 9 rocket was launched from Vandenberg Space Force Base in California, USA, its burned propellant was visible over a much wider area, with the featured photograph being taken from Akureyri, Iceland. The huge spaceship was lifted off a week ago, and the resulting spectacle was captured soon afterward with a single 10-second smartphone exposure, before it quickly dissipated. Like noctilucent clouds, the plume's brightness is caused 
        by the Twilight Effect, where an object is high enough to be illuminated by the twilight Sun, even when the observer on the ground experiences the darkness of night. The 
        spiral shape is caused by the Falcon  rocket reorienting to release satellites in different directions. Stars and faint green and red aurora appear in the background of this extraordinary image.",
        'hdurl': 'https://apod.nasa.gov/apod/image/2403/RocketSpiral_Yang_3024.jpg', 
        'media_type': 'image',
        'service_version': 'v1', 
        'title': 'A Galaxy-Shaped Rocket Exhaust Spiral',
        'url': 'https://apod.nasa.gov/apod/image/2403/RocketSpiral_Yang_960.jpg'
        }
    ''' 

    if response['media_type']== 'image':
        icon_response = requests.get(url,stream=True)
        get_pic = icon_response.content
        #print(get_pic)
        convert_pic = BytesIO(get_pic)
        #print(convert_pic)
        smallpic=Image.open(convert_pic)
        thumb = Image.open(convert_pic)
        thumb.thumbnail((200,200))
        thumb = ImageTk.PhotoImage(thumb)
        picture_label.config(image=thumb)
    elif response['media_type']== 'video':
        picture_label.config(text=url,image='')
        webbrowser.open(url)
    
#full pic

def fullPic():

    global my_img
    global hd_pic

    url = response['hdurl']
    #print(url)
    get_response=requests.get(url,stream=True)
    get_hd_pic=get_response.content
    converted_hd_pic=BytesIO(get_hd_pic)
    
    hd_pic = Image.open(converted_hd_pic)

    #crete new window to dsiplay picture in hd 
    question = messagebox.askyesno('Open pic',"Do you want to open Hd Picture")
    if question == 1:
        my_img=ImageTk.PhotoImage(hd_pic)
        #full_pic_label=tkinter.Label(popup_full,image=my_img)
    else:
        my_img=ImageTk.PhotoImage(smallpic)
        #full_pic_label=tkinter.Label(popup_full,image=my_img)
    
    popup_full=tkinter.Toplevel()
    popup_full.title("Hd Picture")
    popup_full.iconbitmap('rocket.ico')
    popup_full.config(bg=nasa_white)
    full_pic_label=tkinter.Label(popup_full,image=my_img)
    full_pic_label.pack()

def save_pic():
    question = messagebox.askyesno('Save Pic',"Do you want to save normal Picture")
    if question == 1:
        print('save pic')
        save_name = filedialog.asksaveasfilename(initialdir="./",title='Save Pic',filetypes=(("JPEG",".jpg"),("All files","*.*")))
        smallpic.save(save_name +".jpg")
    else:
        fullPic()
        save_name = filedialog.asksaveasfilename(initialdir="./",title='Save Pic',filetypes=(("JPEG",".jpg"),("All files","*.*")))
        hd_pic.save(save_name +".jpg")


#layout 
input_frame=tkinter.Frame(root,bg=nasa_blue)
output_frame=tkinter.Frame(root,bg="white")

input_frame.pack()
output_frame.pack(padx=50, pady=(0,25))

#input frame 
calendar = DateEntry(input_frame,font=text_font,background=nasa_blue,foreground=nasa_light_blue)
submit_button=tkinter.Button(input_frame,text='Submit',font=text_font,bg=nasa_light_blue,command = submit )
full_button=tkinter.Button(input_frame,text='Full Photo',font=text_font,bg=nasa_light_blue,command=fullPic)
save_button=tkinter.Button(input_frame,text='Save photo',font=text_font,bg=nasa_light_blue,command=save_pic)
quit_button=tkinter.Button(input_frame,text='Exit',font=text_font,bg=nasa_red,command=root.destroy)


calendar.grid(row=0,column=0,padx=5,pady=10)
submit_button.grid(row=0,column=1,padx=5,pady=10,ipadx=35)
full_button.grid(row=0,column=2,padx=5,pady=10,ipadx=25)
save_button.grid(row=0,column=3,padx=5,pady=10,ipadx=25)
quit_button.grid(row=0,column=4,padx=5,pady=10,ipadx=50)



#output frame 
picture_date=tkinter.Label(output_frame,bg="white",font=text_font)
picture_explanation=tkinter.Label(output_frame,bg="white",font=text_font,wraplength=600)
picture_label=tkinter.Label(output_frame,bg="white")

#picture_date.grid(row=1,column=1)
#picture_explanation.grid(row=0,column=0,pady=2)
#picture_label.grid(row=0,column=1,padx=10,pady=2)

picture_date.grid(row=1, column=1, padx=10)
picture_explanation.grid(row=0, column=0, padx=10, pady=10, rowspan=2)
picture_label.grid(row=0, column=1, padx=10, pady=10)


submit()
#main loop
root.mainloop()