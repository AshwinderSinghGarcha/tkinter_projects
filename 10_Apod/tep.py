import tkinter as tk
from tkcalendar import DateEntry
import requests
from PIL import ImageTk, Image
from io import BytesIO
 
# Define window
root = tk.Tk()
root.title('APOD Viewer')
root.iconbitmap('rocket.ico')
 
# Define fonts and colors
text_font = ('Times New Roman', 14)
nasa_blue = "#043c93"
nasa_light_blue = "#7aa5d3"
nasa_red = "#ff1923"
nasa_white = "#ffffff"
root.config(bg=nasa_blue) 
 
# Define functions
 
def get_request():
    """Get request data from NASA APOD API"""
    global response
 
    # Set the parameters for the request
    url = 'https://api.nasa.gov/planetary/apod'
    api_key = 'jzl7cIF5J1mejsYPSQAWpfUELaBxlxYKWZULQScz'
    date = calendar.get_date()
 
    querystring = {'api_key':api_key, 'date':date}
 
    # Call the request and turn it into a python usable format
    response = requests.request('GET', url, params=querystring)
    response = response.json()
    print(response)
    # Update output labels
    set_info()
 
def set_info():
    """Update output labels based on API call"""
 
    # Example Response
    """{'copyright': '\nCari Letelier\n', 'date': '2023-07-04',
    'explanation': "It seemed like the sky exploded. The original idea was to photograph an aurora over a waterfall.
    After waiting for hours under opaque clouds, though, hope was running out. Others left. Then, unexpectedly,
    the clouds moved away. Suddenly, particles from a large solar magnetic storm were visible impacting the Earth's upper atmosphere with
    full effect. The night sky filled with colors and motion in a thrilling auroral display.  
    Struggling to steady the camera from high Earthly winds, the 34 exposures that compose the featured image were taken.  
    The resulting featured composite image shows the photogenic Godafoss (Goðafoss) waterfall in northern Iceland in front of 
    a very active aurora in late February. The solar surface explosion that expelled the energetic particles occurred a few days before.  
    Our Sun is showing an impressive 
    amount of surface activity as it approaches solar maximum, indicating that more impressive auroras are likely to appear 
    in Earth's northern and southern sky over the next few years.",
    'hdurl': 'https://apod.nasa.gov/apod/image/2307/WaterfallAurora_Letelier_1600.jpg',
    'media_type': 'image', 'service_version': 'v1', 'title': 'Aurora over Icelandic Waterfall',
    'url': 'https://apod.nasa.gov/apod/image/2307/WaterfallAurora_Letelier_960.jpg'}"""
 
    # Update the picture date and explanation
    picture_date.config(text=response['date']) 
    picture_explanation.config(text=response['explanation']) 
    # picture_date.config(text=response['date']) 
 
    # We need to use 3 images in other functions; an image, a thumbnail, and a full image
    global img
    global thumb
    global full_img
 
    # Grab the photo that is stored in our response
    url = response['url']
    print(url)
    img_response = requests.get(url, stream=True)
 
    # Get the content of the response and use BytesIO to open it as an image
    # Keep a reference to this image  as this is what we can use to save the image (Image not PhotoImage)
    # Create the full screen image for a second window
    img_data = img_response.content
    img = Image.open(BytesIO(img_data))
 
    full_img = ImageTk.PhotoImage(img)
 
    # Create the thumbnail to the main screen
    thumb_data = img_response.content
    thumb = Image.open(BytesIO(thumb_data))
    thumb.thumbnail((200, 200))
    thumb = ImageTk.PhotoImage(thumb)
 
    # Set the thumbnail image
    picture_label.config(image= thumb)
 
 
# Define Layout
# Create frames
input_frame = tk.Frame(root, bg=nasa_blue)
output_frame = tk.Frame(root, bg=nasa_blue)
input_frame.pack()
output_frame.pack(padx=50, pady=(0,25))
 
#Layout for the input frame
calendar = DateEntry(input_frame, width=10, font=text_font, background=nasa_blue, foreground=nasa_white)
submit_button = tk.Button(input_frame, text="Submit", font=text_font, bg=nasa_light_blue, command=get_request)
full_button = tk.Button(input_frame, text="Full Photo", font=text_font, bg=nasa_light_blue)
save_button = tk.Button(input_frame, text="Save Photo", font=text_font, bg=nasa_light_blue)
quit_button = tk.Button(input_frame, text="Exit", font=text_font, bg=nasa_red, command=root.destroy)
 
calendar.grid(row=0, column=0, padx=5, pady=10)
submit_button.grid(row=0, column=1, padx=5, pady=10, ipadx=35)
full_button.grid(row=0, column=2, padx=5, pady=10, ipadx=25)
save_button.grid(row=0, column=3, padx=5, pady=10, ipadx=25)
quit_button.grid(row=0, column=4, padx=5, pady=10, ipadx=50)
 
# Layout for the outputframe
picture_date = tk.Label(output_frame, font=text_font, bg=nasa_white)
picture_explanation = tk.Label(output_frame, font=text_font, bg=nasa_white, wraplength=600)
picture_label = tk.Label(output_frame)
 
picture_date.grid(row=1, column=1, padx=10)
picture_explanation.grid(row=0, column=0, padx=10, pady=10, rowspan=2)
picture_label.grid(row=0, column=1, padx=10, pady=10)
 
# Run the root window's main loop
root.mainloop()
 