#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH  date 20 feb 2024
# Audacity program to create dash and dot mp3
#Icon Found from https://icons8.com/icons


import tkinter 
from tkinter import IntVar, ACTIVE, NORMAL
from tkinter import DISABLED
import random

#main window 
root=tkinter.Tk()
root.title('Garcha Memory Game')
root.iconbitmap('Simon.ico')
root.geometry('400x400')
root.resizable(0,0)

#fonts 
font1=('Arial',12)
font2=('Arial',8)
gray='#550527'
gr_light="#FB0A6E"
purple='#90189e'
pu_light="#f802f9"
root_color='#22a79e'
red='#078384'
re_light="#00fafa"
biscuit='#9ba00f'
bi_light="#f7f801"
root.config(bg=root_color)

#function to disable all button 
def disable_button():
    ist_b.config(state=DISABLED)
    snd_b.config(state=DISABLED)
    trd_b.config(state=DISABLED)
    fth_b.config(state=DISABLED)

def Normal_button():
    ist_b.config(state=NORMAL)
    snd_b.config(state=NORMAL)
    trd_b.config(state=NORMAL)
    fth_b.config(state=NORMAL)

#function
def new_game():
    #disable buttons until random colour finish flashing and reenable after
    disable_button()
    global random_number
    update_label("Playing!")
    random_number.clear()
    while len(random_number)<4:
        n = random.randint(0,3)
        if len(random_number) == 0:
            random_number.append(n)
            
        elif n != random_number[-1]:
                #for i in range(0,4):    
                random_number.append(n)
                
           
    print(random_number)
    
    time_get()
    #print(time)
    
    delay=0
    for value in random_number:
        if value == 0:
             #print(value)
             root.after(delay,lambda:animate(ist_b))
             #ist_b.config(state=ACTIVE)
             #root.after(delay)
             #ist_b.config(state=NORMAL)
             #root.after(time)
             #print(value)
        elif value == 1:
             #print(value)
             root.after(delay,lambda:animate(snd_b))
             #snd_b.config(state=ACTIVE)
             #root.after(delay)
             #snd_b.config(state=NORMAL)     
             #root.after(delay)
        elif value == 2:
             #print(value)
             root.after(delay,lambda:animate(trd_b))
             #trd_b.config(state=ACTIVE)
             #root.after(delay)
             #trd_b.config(state=NORMAL)   
             #root.after(delay)
        elif value == 3:
             #print(value)
             root.after(delay,lambda:animate(fth_b))
             #fth_b.config(state=ACTIVE)
             #root.after(delay)
             #fth_b.config(state=NORMAL)
             #root.after(delay)
     
        delay += time 
    #Normal_button()     
    

def animate(button):
    button.config(state=ACTIVE)
    root.after(time,lambda :button.config(state=NORMAL))

def time_get():
    global time
    if level.get()==0:
        #print('Easy')
        time=1000
    elif level.get()==1:
        #print('Med')
        time=400
    elif level.get()==2:
        #print('Hard')
        time=100

def user_input(number):
    global user_list
    global result
      
    if len(user_list)<4:
        user_list.append(number)
        print(user_list)    
    else:
        user_list.clear()
        user_list.append(number)
        print(user_list) 
    
    if len(user_list) == 4:
        if user_list == random_number:
            print('winner')
            update_label("Right")
            root.after(time,new_game)
            
            result+=10
            print(result)
            score_o.config(text=str(result))
            disable_button()
           # if result >20:
            #    level.set(1)
            
            #if result >50:
            #    level.set(2)   

            random_number.clear()
        else:
            
            print('Loser')
            if result > 9:
                result-=10
                score_o.config(text=str(result))
                update_label("Wrong")
                disable_button()
                root.after(time,new_game)
                print(result)
               # new_game_button.config(text='Wrong')
            else :
                result =0
                user_list.clear()
                score_o.config(text="LOST")
                new_game_button.config(text="New Game",bg='White')
                disable_button()
                
    else:
        print('please add colours')

def update_label(new_label):
    new_game_button.config(text=new_label)

    if new_label == "Wrong":
        new_game_button.config(bg='red')
    elif new_label == "Right":
        new_game_button.config(bg='Yellow')
    elif new_label == "Playing!":
        new_game_button.config(bg='GREEN')
    elif new_label == "Playing!":
        new_game_button.config(bg='GREEN')
    #new_game_button.config(text='New Game')

#layout 
top_frame=tkinter.Frame(root,bg=root_color)
bot_frame=tkinter.LabelFrame(root,bg='white',width=100,height=100)
top_frame.pack()
bot_frame.pack(pady=(5,0))

#topframe button
new_game_button = tkinter.Button(top_frame,text='New Game',font=font1,command=new_game)
Score=tkinter.Label(top_frame,text='Score:',bg=root_color,font=font1)
score_o=tkinter.Label(top_frame,text='0',bg=root_color,font=font1)

new_game_button.grid(row=0,column=0,columnspan=2,padx=10,pady=10,ipadx=30,sticky='WE')
Score.grid(row=0,column=2,pady=10)
score_o.grid(row=0,column=3,pady=10)

#button Frame button 
ist_b=tkinter.Button(bot_frame,bg=gray,activebackground=gr_light,borderwidth=5,command=lambda:user_input(0),state=DISABLED)
snd_b=tkinter.Button(bot_frame,bg=purple,activebackground=pu_light,borderwidth=5,command=lambda:user_input(1),state=DISABLED)
trd_b=tkinter.Button(bot_frame,bg=red,activebackground=re_light,borderwidth=5,command=lambda:user_input(2),state=DISABLED)
fth_b=tkinter.Button(bot_frame,bg=biscuit,activebackground=bi_light,borderwidth=5,command=lambda:user_input(3),state=DISABLED)

ist_b.grid(row=0,column=0,columnspan=2,padx=10,pady=10,ipadx=60,ipady=50,sticky="WE")
snd_b.grid(row=0,column=2,columnspan=2,padx=10,pady=10,ipadx=60,ipady=50,sticky="WE")
trd_b.grid(row=1,column=0,columnspan=2,padx=10,pady=10,ipadx=60,ipady=50,sticky="WE")
fth_b.grid(row=1,column=2,columnspan=2,padx=10,pady=10,ipadx=60,ipady=50,sticky="WE")


#radio
user_list=[]
random_number=[]
result=0
level= IntVar()
level.set(0)
time =500
diff_label=tkinter.Label(bot_frame,text='Difficulity',font=font2)
easy_radio=tkinter.Radiobutton(bot_frame,text='Easy',variable = level,value=0,font=font2)
medi_radio=tkinter.Radiobutton(bot_frame,text='Medium',variable = level,value=1,font=font2)
hard_radio=tkinter.Radiobutton(bot_frame,text='Hard',variable = level,value=2,font=font2)

diff_label.grid(row=2,column=0,padx=5)
easy_radio.grid(row=2,column=1)
medi_radio.grid(row=2,column=2)
hard_radio.grid(row=2,column=3)
#radio button 

#main loop
root.mainloop()