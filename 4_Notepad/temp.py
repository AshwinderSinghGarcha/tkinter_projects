#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH  date 18 feb 2024
#icon https://doublejdesign.co.uk/

import tkinter 
from PIL import ImageTk,Image
from tkinter import StringVar,IntVar
from tkinter import font ,END,messagebox,filedialog
from tkinter.scrolledtext import ScrolledText
#main window 
root = tkinter.Tk()
root.title("Notepad")
root.iconbitmap('pad.ico')
root.geometry('600x700')
root.resizable(0,0)

#colors and font 
root_color='#bdc3c7'
input_colour='#5f6e7c'
output_colour='#ebf3f3'
root.config(bg=root_color)

#define functions 
#define close 
def close_note():
    question=messagebox.askyesno('Close Note','Do you want to Close note')
    if question == 1:
        root.destroy()

#define new
def new_note():
    question=messagebox.askyesno('New Note',"Do you want to open new note")
    if question==1:
        output_text.delete('1.0',END)
 
#define font update 
def font_update(event):
    #take all reading in font families ,size , option 
    if opt_var.get() == 'none':
        my_font=(family_var.get(),size_var.get())
    else:
        my_font=(family_var.get(),size_var.get(),opt_var.get())
    
    #chanfe font 
    output_text.config(font=my_font)


#define save 
def save_note():
    file_path=filedialog.asksaveasfilename(
                                            initialdir='./', # this is for current directory
                                            title='Save note', # this is to name dialog box 
                                            filetypes=(("Text files", "*.txt"),("All files", "*.*")) #this is to give two option text or all files
                                          )
    with open (file_path,'w') as my_file:
        my_file.write(family_var.get()+'\n')
        my_file.write(str(size_var.get())+'\n')
        my_file.write(opt_var.get()+'\n')
        my_file.write(output_text.get('1.0',END))

#define open
def open_note():
    file_path= filedialog.askopenfilename(
                                             initialdir='./', # this is for current directory
                                             title='Open note', # this is to name dialog box 
                                            filetypes=(('Text files','*.txt'),('All files','*.*')) #this is to give two option text or all files
                                         )
    with open (file_path,'r') as my_file:
        output_text.delete("1.0",END)# this is for clear output before print
        family_var.set(my_file.readline().strip())
        size_var.set(my_file.readline().strip())
        opt_var.set(my_file.readline().strip())
        font_update(1) # updaet font from text file 
        output_text.insert("1.0",my_file.read())

#frames  in pack format 
input_frame=tkinter.Frame(root,bg=input_colour)
output_frame=tkinter.Frame(root)
input_frame.pack(padx=5,pady=5)
output_frame.pack(padx=5,pady=5)


#input frame layout 

#add images to buttons 
new_img=ImageTk.PhotoImage(Image.open('new.png'))
open_img=ImageTk.PhotoImage(Image.open('open.png'))
save_img=ImageTk.PhotoImage(Image.open('save.png'))
close_img=ImageTk.PhotoImage(Image.open('close.png'))
#buttons 
new_btn=tkinter.Button(input_frame,image=new_img,command=new_note)
open_btn=tkinter.Button(input_frame,image=open_img,command=open_note)
save_btn=tkinter.Button(input_frame,image=save_img,command=save_note)
close_btn=tkinter.Button(input_frame,image=close_img,command=close_note)

new_btn.grid(row=0,column=0,padx=5,pady=5)
open_btn.grid(row=0,column=1,padx=5,pady=5)
save_btn.grid(row=0,column=2,padx=5,pady=5)
close_btn.grid(row=0,column=3,padx=5,pady=5)

#drop menu (either use combobox or optionmenu )
#test code to see all fonts in python library font 
#font_list = font.families()
#font_family=tkinter.OptionMenu(input_frame,family_var,*font_list)
my_font_family=['Terminal','Script','Cambria','Roman','Courier New CE','Segoe UI']
my_font_size=[2,8,12,24,48,64,128]
my_font_opt=['none','bold','italic']

#define variable type to store intvar and stringVar
family_var= StringVar()
size_var=IntVar()
opt_var=StringVar()

font_family=tkinter.OptionMenu(input_frame,family_var,*my_font_family,command=font_update)
font_size=tkinter.OptionMenu(input_frame,size_var,*my_font_size,command=font_update)
font_opt=tkinter.OptionMenu(input_frame,opt_var,*my_font_opt,command=font_update)

#set by default value , width of drop menu
family_var.set('Terminal')
size_var.set(2)
opt_var.set('none')

font_family.config(width=16)
font_size.config(width=2)
font_opt.config(width=5)

font_family.grid(row=0,column=4,padx=5,pady=5)
font_size.grid(row=0,column=5,padx=5,pady=5)
font_opt.grid(row=0,column=6,padx=5,pady=5)

#output frame layout 

#use listbox to display output,prob is we need entry box and a button to display and add text , we also need scroll bar 
#output_text=tkinter.Listbox(output_frame,width=1000,height=1000,bg=output_colour)
#output_text.pack(padx=5,pady=5)
my_font=(family_var.get(),size_var.get())
#use scrolltext widget
output_text=ScrolledText(output_frame,width=1000,height=1000,bg=output_colour,font=my_font)
output_text.pack(padx=5,pady=5)

#main loop
root.mainloop()