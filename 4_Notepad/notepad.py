#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH  date 17 feb 2024
#icon https://doublejdesign.co.uk/

import tkinter
from PIL import ImageTk,Image
from tkinter import StringVar, IntVar, scrolledtext,END ,messagebox, filedialog

#define winodw
root=tkinter.Tk()
root.title('Garcha Notepad')
root.iconbitmap('pad.ico')
root.geometry('600x600')
root.resizable(0,0)

#deine fonts and colors 
text_color='#fffacd'
menu_color='#dbd9db'
root_color='#6c809a'
root.config(bg=root_color)


#define functions
def change_font(event):
        if font_options.get() == 'none':
            font=(font_family.get(),font_size.get())
        else:
            font=(font_family.get(),font_size.get(),font_options.get())
        
        output_text.config(font=font)


#define new note 
def new_note():
    #use message box to ask for a new note 
    question=messagebox.askyesno("New Note","Are you sure want to start a new note")
    if question == 1:
    #scrolled text starting index is o.1
        output_text.delete("1.0",END)

#define close  
def destroy():
    #use message box to ask for a new note 
    question=messagebox.askyesno("New Note","Are you sure want to close note")
    if question == 1:
    #scrolled text starting index is o.1
        root.destroy()

#save file 
def save():
    #use file dialog to get location and name where to save 
    save_name=filedialog.asksaveasfilename(initialdir='./',title='Save note',filetypes=(("text Files","*.txt"),("All Files","*.*")))
    with open(save_name, "w") as my_file:
        my_file.write(font_family.get()+"\n")
        my_file.write(str(font_size.get())+"\n")
        my_file.write(font_options.get()+"\n")
        my_file.write(output_text.get("1.0",END))

# open file 
def open_note():
    open_file = filedialog.askopenfilename(initialdir='./',title='Open note',filetypes=(("text Files","*.txt"),("All Files","*.*")))
    with open(open_file,"r") as my_file:
        #before display clear output field 
        output_text.delete("1.0",END)
        #set font family size and option using reading first three line and use strip to remove \n
        font_family.set(my_file.readline().strip())
        font_size.set(int(my_file.readline().strip()))
        font_options.set(my_file.readline().strip())
        #use function to update font 
        change_font(1)
        #now read rest file and display on screen
        text=my_file.read()
        output_text.insert("1.0",text)



#define layout
#define frames 
menu_frame=tkinter.Frame(root,bg=menu_color)
text_frame=tkinter.Frame(root,bg=text_color)
menu_frame.pack(padx=5,pady=5)
text_frame.pack(padx=5,pady=5)

#layout for menu frame
#define open save close new font family font size font option
new_img=ImageTk.PhotoImage(Image.open('new.png'))
open_img=ImageTk.PhotoImage(Image.open('open.png'))
save_img=ImageTk.PhotoImage(Image.open('save.png'))
close_img=ImageTk.PhotoImage(Image.open('close.png'))

new_button=tkinter.Button(menu_frame,image=new_img,command=new_note)
open_button=tkinter.Button(menu_frame,image=open_img,command=open_note)
save_button=tkinter.Button(menu_frame,image=save_img,command=save)
close_button=tkinter.Button(menu_frame,image=close_img,command=destroy)
new_button.grid(row=0,column=0,padx=5,pady=5)
open_button.grid(row=0,column=1,padx=5,pady=5)
save_button.grid(row=0,column=2,padx=5,pady=5)
close_button.grid(row=0,column=3,padx=5,pady=5)

#create listof fonts 
my_font= ['Terminal', 'Modern', 'Script', 'Courier', 'Arial', 'Calibri', 'Cambria',
'Georgia', 'MS Gothic', 'SimSun', 'Tahoma', 'Times New Roman', 'Verdana', 'Wingdings']
font_family = StringVar()
font_family_drop = tkinter.OptionMenu(menu_frame,font_family,*my_font,command=change_font)
font_family.set('Terminal')
font_family_drop.config(width=16)
font_family_drop.grid(row=0,column=4,padx=5,pady=5)

#create lis tof size 
my_fontsize=[8, 10, 12, 14, 16, 20, 24, 32, 48, 64, 72, 96]
font_size = IntVar()
font_size_drop=tkinter.OptionMenu(menu_frame,font_size,*my_fontsize,command=change_font)
font_size.set(12)
font_size_drop.config(width=2)
font_size_drop.grid(row=0,column=5,padx=5,pady=5)

#create font type 
my_options=['none','bold','italic']
font_options = StringVar()
font_option_drop=tkinter.OptionMenu(menu_frame,font_options,*my_options,command=change_font)
font_options.set('none')
font_option_drop.config(width=5)
font_option_drop.grid(row=0,column=6,padx=5,pady=5)

#layout for text frame
font=(font_family.get(),font_size.get())
output_text= tkinter.scrolledtext.ScrolledText(text_frame,width=1000,height=1000,bg=text_color,font=font)
output_text.pack()


#main loop
root.mainloop()