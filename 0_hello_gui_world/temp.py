
#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 

# icon website https://www.iconarchive.com/
# colour themes  https://coolors.co/

import tkinter 
from tkinter import StringVar 
from tkinter import END ,BOTH
from PIL import Image,ImageTk

root=tkinter.Tk()
root.title('myapp')
root.iconbitmap("wave.ico")
root.geometry("400x400")
root.resizable(0,0)
root.config(bg="#A675A1")

input_col="#E2AFDE"
output_col="#F991CC"


#define function
def make_label():

    if char_string.get() =="lower":
        output_label=tkinter.Label(output_F,text=text_en.get(),bg=output_col)
        
    
    elif char_string.get() =="Upper":
        output_label=tkinter.Label(output_F,text=(text_en.get()).upper(),bg=output_col)
    
    output_label.pack()
    text_en.delete(0,END)



input_F=tkinter.LabelFrame(root,width=400,height=100,bg=input_col)
output_F=tkinter.LabelFrame(root,width=400,height=300,bg=output_col)
input_F.pack(padx=10,pady=10)
output_F.pack(padx=10,pady=(0,10),fill=BOTH,expand=True)


text_en=tkinter.Entry(input_F,width=10)
text_en.grid(row=0,column=0,padx=10,pady=10)

submit=tkinter.Button(input_F,text="Submit",command=make_label)
submit.grid(row=0,column=1,padx=10,pady=10,ipadx=10)

char_string = StringVar()
char_string.set('lower')
Upper_char=tkinter.Radiobutton(input_F,text="Upper case",variable=char_string,value="Upper",bg=input_col)
lower_char=tkinter.Radiobutton(input_F,text="lower case",variable=char_string,value="lower",bg=input_col)

Upper_char.grid(row=1,column=0,padx=10,pady=10)
lower_char.grid(row=1,column=1,padx=10,pady=10)

my_image=ImageTk.PhotoImage(Image.open('smile.png'))
outpt_image=tkinter.Label(output_F,image=my_image)
outpt_image.pack()


root.mainloop()