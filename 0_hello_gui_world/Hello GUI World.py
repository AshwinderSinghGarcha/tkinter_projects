#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 

# icon website https://www.iconarchive.com/
# colour themes  https://coolors.co/
import tkinter 
from tkinter import StringVar
from tkinter import BOTH,END
from PIL import ImageTk,Image

#define fonts and colours 
root_colour   ="#224870"
input_colour  ="#2a4494"
output_colour ="#4ea5d9"

#define windows
root=tkinter.Tk()
root.title("HELLO GUI WORLD")
root.iconbitmap("wave.ico")
root.geometry("400x400")
root.resizable(0,0)
root.config(bg=root_colour)

#define function
def Make_submit():
    
    if case_style.get() == "normal":
        output_label=tkinter.Label(output_frame,text="Hello "+ input_entry.get(),bg=output_colour)

    elif case_style.get() =="Upper":
         output_label=tkinter.Label(output_frame,text=(input_entry.get()).upper(),bg=output_colour)   
    
    output_label.pack()
    input_entry.delete(0,END)

#define layout
#define frames
input_frame  =tkinter.LabelFrame(root,width=400,height=100,bg=input_colour)
output_frame =tkinter.LabelFrame(root,width=400,height=300,bg=output_colour)

input_frame.pack(padx=10,pady=10)
output_frame.pack(padx=10,pady=(0,10),fill=BOTH, expand=True)

#define radio buttons and print button
case_style = StringVar()
case_style.set('normal')
radio_button_1 = tkinter.Radiobutton(input_frame,text='Normal Case',borderwidth=5,bg=input_colour,variable =case_style,value="normal")
radio_button_2 = tkinter.Radiobutton(input_frame,text="Upper Case",borderwidth=5,bg=input_colour,variable =case_style,value="Upper")
Submit_button_1 = tkinter.Button(input_frame,text='Submit',command=Make_submit)

Submit_button_1.grid(row=0,column=1,padx=10,pady=10,ipadx=20)
radio_button_1.grid(row=1,column=0,padx=5,pady=5)
radio_button_2.grid(row=1,column=1,padx=5,pady=5)

#ENTRY WIDGET
input_entry = tkinter.Entry(input_frame,text="Enter your name",width=20)
input_entry.grid(row=0,column=0,padx=10,pady=10)

#output widget 
smile_img = ImageTk.PhotoImage(Image.open('smile.png'))
smile_label = tkinter.Label(output_frame,image=smile_img,bg=output_colour)
smile_label.pack()

#Run root window 
root.mainloop()

