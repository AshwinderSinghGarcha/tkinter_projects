#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH  date 20 feb 2024
# Audacity program to create dash and dot mp3
#Icon Found from https://icons8.com/icons

import tkinter 
from tkinter import IntVar,END,DISABLED,NORMAL
from playsound import playsound
from PIL import ImageTk,Image

#main window
root = tkinter.Tk()
root.title("GARCHA Morse Code Translator")
root.iconbitmap('morse.ico')
root.geometry('500x350')
root.resizable(0,0)


#define fonts and colors
buttton_font=('Simsun',10)
frame_color = '#dcdcdc'
root_color='#778899'
button_color='#c0c0c0'
text_color='#f8f8ff'
root.config(bg=root_color)

#define functions
def convert():
    if lang.get() ==1:
        #take input
        #print('1')
        input = str(text_input.get("1.0",END).strip("\n"))
        input=input.lower()

        text_output.delete('1.0',END)
        for item in input:
        #use english to morse 
            print(item)
            y=english_to_morse[item]
            z= y+' '
            text_output.insert(END,z)
    
    elif lang.get() == 2:
        #take input 
        print('2')
        input = str(text_input.get('1.0',END).strip("\n"))
        input_list = input.split(" ")
        print(input_list)
        text_output.delete('1.0',END)
        for item in input_list:
        #use english to morse 
            print(item)
            
            y=morse_to_english[item]
            z= y+''
            text_output.insert(END,z)
    #display result in morse 
    
def clear():
    text_output.delete('1.0',END) 


def play():
    #find whwre is morse code 
    if lang.get() ==1:
        text=text_output.get("1.0",END)
    elif lang.get() == 2:
        text=text_input.get("1.0",END)
    #play tone (.,-,"",|)
    for value in text:
        if value == ".":
            playsound('dot.mp3')
            root.after(100)
        elif value == "-":
            playsound('dash.mp3')
            root.after(200)
        elif value == " ":
            root.after(300)
        elif value == "|":
            root.after(700)


def show_guide():

    global guid
    global my_image

    guid=tkinter.Toplevel()
    guid.title('Morse guide')
    guid.iconbitmap('morse.ico')
    guid.geometry('350x350+'+ str(root.winfo_x()+500)+ "+" + str(root.winfo_y()))
    guid.config(bg=root_color)
    guid.resizable(0,0)

    #convert imgae 
    my_image = ImageTk.PhotoImage(Image.open('morse_chart.jpg'))
    my_image_label=tkinter.Label(guid,image=my_image,bg=frame_color)
    my_image_label.pack(padx=10,pady=10,ipadx=5,ipady=5)

    guide_button.config(state=DISABLED)

    close_button=tkinter.Button(guid,text='Close',font=buttton_font,bg=button_color,command=close_guide)
    close_button.pack(padx=10,ipadx=50)

def close_guide():
    guid.destroy()
    guide_button.config(state=NORMAL)


#Create dictionaries 
english_to_morse={
                  'a':'.-',
                  'b':'-...',
                  'c':'-.-.',
                  'd':'-..',
                  'e':'.',
                  'f':'..-.',
                  'g':'--.',
                  'h':'....',
                  'i':'..',
                  'j':'.---',
                  'k':'-.-',
                  'l':'.-..',
                  'm':'--',
                  'n':'-.',
                  'o':'---',
                  'p':'.--.',
                  'q':'--.-',
                  'r':'.-.',
                  's':'...',
                  't':'-',
                  'u':'..-',
                  'v':'...-',
                  'w':'.--',
                  'x':'-..-',
                  'y':'-.--',
                  'z':'--..',
                  '1':'.----',
                  '2':'..---',
                  '3':'...--',
                  '4':'....-',
                  '5':'.....',
                  '6':'-....',
                  '7':'--...',
                  '8':'---..',
                  '9':'----.',
                  '0':'-----',
                  ' ': '/',
                  '|': '|',
                   "": "",
                 }
                    #this methoad use dict comprehension
morse_to_english =  {
                    value:key for key,value in english_to_morse.items()
                    }
                    #this methoad create new dict
morse_to_english_1 =dict(  
                    (value,key) for key,value in english_to_morse.items()
                    )
                     #this methoad create new dict and map()
morse_to_english_2 =dict(  
                    map(reversed,english_to_morse.items())
                    )                     
#print(english_to_morse)
#print(morse_to_english)
#print(morse_to_english_1)
#print(morse_to_english_2)
#layout
input_Frame = tkinter.LabelFrame(root,bg=frame_color)
output_Frame = tkinter.LabelFrame(root,bg=frame_color)
input_Frame.pack(padx=16,pady=(16,8))
output_Frame.pack(padx=16,pady=(8,16))

#input layout 
text_input=tkinter.Text(input_Frame,width=30,height=8,bg=text_color)
text_input.grid(row=0,column=1,rowspan=3,padx=5,pady=5)

lang = IntVar()
lang.set(1)
morse_radio=tkinter.Radiobutton(input_Frame,text="English --> Morse Code",variable=lang,value=1,bg=frame_color,font=buttton_font)
English_radio=tkinter.Radiobutton(input_Frame,text="Morse Code --> English",variable=lang,value=2,bg=frame_color,font=buttton_font)
morse_radio.grid(row=0,column=0,pady=(15,0))
English_radio.grid(row=1,column=0)

guide_button= tkinter.Button(input_Frame,text="Guide",bg=button_color,font=buttton_font,command=show_guide)
guide_button.grid(row=2,column=0,sticky="WE",padx=10)


#output layout 
text_output=tkinter.Text(output_Frame,width=30,height=8,bg=text_color)
text_output.grid(row=0,column=1,rowspan=4,padx=5,pady=5)

Convert_button= tkinter.Button(output_Frame,text="Convert",font=buttton_font,bg=button_color,command=convert)
Play_Morse_button= tkinter.Button(output_Frame,text="Play Morse",font=buttton_font,bg=button_color,command=play)
Clear_button= tkinter.Button(output_Frame,text="Clear",font=buttton_font,bg=button_color,command=clear)
Quit_button= tkinter.Button(output_Frame,text="Quit",font=buttton_font,bg=button_color,command=root.destroy)

Convert_button.grid(row=0,column=0,ipadx=50,padx=10)
Play_Morse_button.grid(row=1,column=0,sticky="WE",padx=10)
Clear_button.grid(row=2,column=0,sticky="WE",padx=10)
Quit_button.grid(row=3,column=0,sticky="WE",padx=10)

#main loop
root.mainloop()