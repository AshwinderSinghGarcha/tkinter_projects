#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 
#colour picker  https://imagecolorpicker.com/

import tkinter
#from tkinter import StringVar
from tkinter import ttk,END
#define Window
root=tkinter.Tk()
root.title('Garcha Metric Helper')
root.iconbitmap(default='ruler.ico')
root.resizable(0,0)



#define Fonts and colours
field_font=('Cambria',10)
bg_colour = '#c75c5c'
button_colour = '#f5cf87'
root.config(bg=bg_colour)

#define functions
def convert():
    '''Convert from one metric prefix to another '''
    metric_values = {
             'femto':10**-15,
             'pico':10**-12,
             'nano':10**-9,
             'micro':10**-6,
             'milli':10**-3,
             'centi':10**-2,
             'deci':10**-1,
             'base value':10**0,
             'deca':10**1,
             'hecto':10**2,
             'kilo':10**3,
             'mega':10**6,
             'giga':10**9,
             'tera':10**12,
             'peta':10**15,

         }
    #clear output field 
    output_field.delete(0,END)
    user_enter=float(input_field.get())
    input_unit=input_combobox.get()
    output_unit=output_combobox.get()

    #math
    base_value=user_enter*metric_values[input_unit]

    end_value=base_value/metric_values[output_unit]
    
    output_field.insert(0,str(end_value))

#define layout
input_field  = tkinter.Entry(root,width=20,font=field_font,borderwidth=3)
output_field = tkinter.Entry(root,width=20,font=field_font,borderwidth=3)
equal_label  = tkinter.Label(root,text='=',bg=bg_colour)

input_field.grid(row=0,column=0,padx=10,pady=10)
output_field.grid(row=0,column=2,pady=10,padx=10)
equal_label.grid(row=0,column=1,padx=10,pady=10)


input_field.insert(0,'Enter your quantity')

 
metric_list = ['femto','pico','nano','micro','milli','centi','deci','base value','deca','hecto','kilo','mega','giga','tera','peta']

#create dropdown for metric values
#input_choice=StringVar()
#output_choice=StringVar()

#input_dropdown=tkinter.OptionMenu(root,input_choice,*metric_list)
#output_dropdown=tkinter.OptionMenu(root,output_choice,*metric_list)

to_label = tkinter.Label(root,text='to',font=field_font,bg=bg_colour)

#input_dropdown.grid(row=1,column=0)
to_label.grid(row=1,column=1,padx=10,pady=10)
#output_dropdown.grid(row=1,column=2)

#input_choice.set('base value')
#output_choice.set('base value')

#create combo box 
input_combobox=ttk.Combobox(root,value=metric_list,font=input_field,justify='center')
output_combobox=ttk.Combobox(root,value=metric_list,font=input_field,justify='center')
input_combobox.grid(row=1,column=0,padx=10,pady=10)
output_combobox.grid(row=1,column=2,padx=10,pady=10)

input_combobox.set("base value")
output_combobox.set("base value")
#create button
convert_button=tkinter.Button(root,text='convert',bg=button_colour,font=field_font,command=convert)
convert_button.grid(row=2,column=0,columnspan=3,padx=10,pady=10,ipadx=50)


#root window main loop
root.mainloop()