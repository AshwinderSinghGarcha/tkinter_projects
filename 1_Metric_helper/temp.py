#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 
#colour picker  https://imagecolorpicker.com/
import tkinter
from tkinter import END,StringVar,ttk

root=tkinter.Tk()
root.title("Metric Calculator")
root.iconbitmap('ruler.ico')
root.resizable(0,0)
root.config(bg='#f5cf87')


def convert():
    output_entry.delete(0,END)
    Metric = {
        'peta':10**15,
        'tera':10**12,
        'giga':10**9,
        'mega':10**6,
        'kilo':10**3,
        'hecto':10**2,
        'deca':10**1,
        'base value':10**0,
        'deci':10**-1,
        'centi':10**-2,
        'milli':10**-3,
        'micro':10**-6,
        'nano':10**-9,
        'pico':10**-12,
        'femto':10**-15,

    }
    entered_number=float(input_entry.get())
    first_conversion = entered_number*Metric[input_combo.get()]
    output=first_conversion/Metric[output_combo.get()]
    output_entry.insert(0,str(output))
    





input_entry= tkinter.Entry(root,width=20,borderwidth=3)
output_entry=tkinter.Entry(root,width=20,borderwidth=3)
equal_label=tkinter.Label(root,text="=",bg='#f5cf87')
input_entry.grid(row=0,column=0,padx=10,pady=10)
output_entry.grid(row=0,column=2,padx=10,pady=10)
equal_label.grid(row=0,column=1,padx=10,pady=10)
input_entry.insert(0,'Enter')
'''
#using option 


input_str=StringVar()
output_str=StringVar()
input_str.set('base value')
output_str.set('base value')
My_list=['femto','pico','nano','micro','milli','centi','deci','base value','deca','hecto','kilo','mega','giga','tera','peta']

input_unit=tkinter.OptionMenu(root,input_str,*My_list)
to_label=tkinter.Label(root,text='to',bg='red')
output_unit=tkinter.OptionMenu(root,input_str,*My_list)

input_unit.grid(row=1,column=0,padx=10,pady=10)
to_label.grid(row=1,column=1,padx=10,pady=10)
output_unit.grid(row=1,column=2,padx=10,pady=10)
'''

My_list=['femto','pico','nano','micro','milli','centi','deci','base value','deca','hecto','kilo','mega','giga','tera','peta']

input_combo=ttk.Combobox(root,value=My_list,justify='center')
output_combo=ttk.Combobox(root,value=My_list,justify='center')

input_combo.grid(row=1,column=0,padx=10,pady=10)
output_combo.grid(row=1,column=2,padx=10,pady=10)
input_combo.set('base value')
output_combo.set('base value')
to_label=tkinter.Label(root,text='to',bg='#f5cf87')
to_label.grid(row=1,column=1,padx=10,pady=10)


Convert_button=tkinter.Button(root,text='Convert',command=convert)
Convert_button.grid(row=2,column=0,columnspan=3,padx=10,pady=10,ipadx=50)




root.mainloop()