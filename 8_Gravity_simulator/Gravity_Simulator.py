#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH  date 2 March 2024
import tkinter
from tkinter import HORIZONTAL ,BOTH ,CURRENT,END
from matplotlib import pyplot

#window 
root = tkinter.Tk()
root.title('Garcha Gravity Demo')
root.iconbitmap('earth.ico')
root.geometry('500x650')
root.resizable(0,0)

time =0
data = {}
for i in range (1,5):
    data['data_%d'% i]=[]

#def function
def move(event):
    if "BALL" in top_canvas.gettags(CURRENT):
        #print(top_canvas.gettags(CURRENT))
        x1=top_canvas.coords(CURRENT)[0]
        x2=top_canvas.coords(CURRENT)[2]
        #y1=top_canvas.coords(CURRENT)[1]
        # y2=top_canvas.coords(CURRENT)[3]
        #print(x1,y1,x2,y2)
        top_canvas.coords(CURRENT,x1,event.y,x2,event.y+10)
        #attempt to not move ball off the canvas
        if top_canvas.coords(CURRENT)[3]<15:
            #print(top_canvas.coords(CURRENT)[3])
            top_canvas.coords(CURRENT,x1,5,x2,15)
        elif top_canvas.coords(CURRENT)[3]>415:
            top_canvas.coords(CURRENT,x1,405,x2,415)
    update_height()

def update_height():
    for i in range (1,5):
        heights['height_%d'%i].config(text='Height: ' + str(round(415-top_canvas.coords(balls['ball_%d'%i])[3], 2)))

def step(t):
    global time 

    #convert a and v to positive using negate because canvas y is 0-400 from top to bottom
    for i in range(1,5):
        a= -1*float(acceleartions['a_%d'%i].get())       # convert to float becz we need to multiply with t slider which is flaot
        v= -1*float(velocities['v_%d'%i].get())
        d= v*t + 0.5*a*t**2
        #print(a,v,d,t)
        
        x1=top_canvas.coords(balls['ball_%d'%i])[0]
        x2=top_canvas.coords(balls['ball_%d'%i])[2]
        #y1=top_canvas.coords(CURRENT)[1]
        # y2=top_canvas.coords(CURRENT)[3]
        #print(x1,y1,x2,y2)
        if top_canvas.coords(balls['ball_%d'%i])[3]+d <= 415:
            top_canvas.move(balls['ball_%d'%i], 0 , d)
            y2=top_canvas.coords(balls['ball_%d'%i])[3]
            top_canvas.create_line(x1,y2,x2,y2,tag='dash')

        else:
             top_canvas.coords(balls['ball_%d'%i],x1,405,x2,415)


        #final velocity 
        vf = v + a*t

        velocities['v_%d'%i].delete(0,END)
        velocities['v_%d'%i].insert(0,str(round(-1*vf,2)))
        update_height()

        #add data to data dict 
        data['data_%d' %i].append((time,415-top_canvas.coords(balls['ball_%d'%i])[3]))
        #for data_list in data['data_%d' %i]:
        #    print(data_list[0])
        #   print(data_list[1])
    #update time
    time +=t

def run():
    step(t_slider.get())

    while 15 < top_canvas.coords(balls['ball_1'])[3] <415 or 15 < top_canvas.coords(balls['ball_2'])[3] <415 or 15 < top_canvas.coords(balls['ball_3'])[3] <415 or 15 < top_canvas.coords(balls['ball_4'])[3] <415 :
     step(t_slider.get())

def graph():
    #dist vs time 
    colors = ['red','green','blue','yellow']
    
    for i in range (1,5):
        x = []    # time in tuple data
        y = []    # height in tuple
        for data_list in data['data_%d' %i]:
            x.append(data_list[0])
            y.append(data_list[1])
        
        pyplot.plot(x,y,color=colors[i-1])
    
    pyplot.title('GARCHA GRAVITY SIMULATION')
    pyplot.xlabel('Time')
    pyplot.ylabel('Distance')
    pyplot.show()

def reset():
    #erase dash lines to line , set balls to gnd , clear entry fields
    time =0 
    top_canvas.delete("dash")

    for i in range(1,5):
        velocities['v_%d'%i].delete(0,END)
        velocities['v_%d'%i].insert(0,'0')

        acceleartions['a_%d'%i].delete(0,END)
        acceleartions['a_%d'%i].insert(0,'0')

        #reset balls to gnd 
        top_canvas.coords(balls['ball_%d'%i],45+(i-1)*100,405,55+(i-1)*100,415)

        # clear data 
        data['data_%d'%i].clear()

    #update label height 
    update_height()
    t_slider.set(1)

#layout
 #frame 
top_frame=tkinter.Frame(root)
Bot_frame=tkinter.Frame(root)
top_frame.pack(pady=10)
Bot_frame.pack(fill=BOTH,expand=True)
  #canvas on top frmae 
top_canvas=tkinter.Canvas(top_frame,width=400,height=415,bg='white')
top_canvas.grid(row=0,column=0,padx=5,pady=5)

Line_0=top_canvas.create_line(2,0,2,415,tag="line")
Line_1=top_canvas.create_line(100,0,100,415,tag="line")
Line_2=top_canvas.create_line(200,0,200,415,tag="line")
Line_3=top_canvas.create_line(300,0,300,415,tag="line")
Line_4=top_canvas.create_line(400,0,400,415,tag="line")

balls = {}
balls['ball_1'] = top_canvas.create_oval(45,405,55,415,fill='red',tag="BALL")
balls['ball_2'] = top_canvas.create_oval(145,405,155,415,fill='green',tag="BALL")
balls['ball_3'] = top_canvas.create_oval(245,405,255,415,fill='blue',tag="BALL")
balls['ball_4'] = top_canvas.create_oval(345,405,355,415,fill='yellow',tag="BALL")


 #bottom frame 

tkinter.Label(Bot_frame,text='d').grid(row=0,column=0)
tkinter.Label(Bot_frame,text='vi').grid(row=1,column=0)
tkinter.Label(Bot_frame,text='a').grid(row=2,column=0,ipadx=22)
tkinter.Label(Bot_frame,text='t').grid(row=3,column=0)

#height label based on balls cordinates 
heights={}
for i in range(1,5):
    heights['height_%d'%i]=tkinter.Label(Bot_frame,text='Height: ' + str(415-top_canvas.coords(balls['ball_%d'%i])[3]))
    heights['height_%d'%i].grid(row=0,column=i)

velocities = {}
for i in range(1,5):
    velocities['v_%d'%i]  = tkinter.Entry(Bot_frame,width=15)
    velocities['v_%d'%i].grid(row=1,column=i,padx=1)
    velocities['v_%d'%i].insert(0,'0')

acceleartions = {}
for i in range(1,5):
    acceleartions['a_%d'%i]  = tkinter.Entry(Bot_frame,width=15)
    acceleartions['a_%d'%i].grid(row=2,column=i,padx=1)
    acceleartions['a_%d'%i].insert(0,'0')

# time slider 
t_slider = tkinter.Scale(Bot_frame,from_=0,to=1,tickinterval=.1,resolution=.01,orient=HORIZONTAL)
t_slider.grid(row=3,column=1,columnspan=4,sticky='WE')
t_slider.set(1)

#buttons 
step_button = tkinter.Button(Bot_frame,text='Step',command=lambda:step(t_slider.get()))
run_button = tkinter.Button(Bot_frame,text='Run',command=run)
graph_button = tkinter.Button(Bot_frame,text='Graph',command=graph)
reset_button = tkinter.Button(Bot_frame,text='Reset',command=reset)
quit_button = tkinter.Button(Bot_frame,text='Quit',command=root.destroy)

step_button.grid(row=4,column=1,pady=(10,0),sticky='WE')
run_button.grid(row=4,column=2,pady=(10,0),sticky='WE')
graph_button.grid(row=4,column=3,pady=(10,0),sticky='WE')
reset_button.grid(row=4,column=4,pady=(10,0),sticky='WE')
quit_button.grid(row=5,column=1,columnspan=4,sticky='WE')

#make each ball dragable using mouse in the vertical direction
root.bind('<B1-Motion>',move)

#main loop
root.mainloop()