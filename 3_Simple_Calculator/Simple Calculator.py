#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 
# colour picker  https://imagecolorpicker.com/
# icon website https://www.iconarchive.com/
# colour themes  https://coolors.co/

import tkinter
from tkinter import END,RIGHT,DISABLED,NORMAL
#define window
root=tkinter.Tk()
root.title('Calculator')
root.iconbitmap("calc.ico")
root.geometry('300x400')
root.resizable(0,0)

#deine colour and font 
dark_green='#93af22'
light_green='#acc253'
white_green='#edefe0'

root.config(bg=white_green)

button_font=('Arial',18)
display_font=('Arial',30)

#define function
def submit_number(number):
    input_entry.insert(END, number)
    if "." in input_entry.get():
        decimal_button.config(state=DISABLED)

def clear_list():
    input_entry.delete(0,END)
    enable_button()


def operate(operator):
    global first_number
    global operation 

    operation=operator
    first_number = input_entry.get()
    input_entry.delete(0,END)
    
    divison_button.config(state=DISABLED)
    multiply_button.config(state=DISABLED)
    add_button.config(state=DISABLED)
    substraction_button.config(state=DISABLED)
    inverse_button.config(state=DISABLED)
    square_button.config(state=DISABLED)
    exponent_button.config(state=DISABLED)
    decimal_button.config(state=NORMAL)
    

def equal():
    
    if operation == 'add':
        output = float(first_number) + float(input_entry.get() )
    elif operation == 'substract':
        output = float(first_number) - float(input_entry.get() )
    elif operation == 'multiply':
        output = float(first_number) * float(input_entry.get() )  
    elif operation == 'divide':
        if input_entry.get()=="0":
            output = "ERROR"
        else:
            output = float(first_number) / float(input_entry.get() )
    elif operation == 'exponent':
        output = float(first_number) ** float(input_entry.get() ) 
    
    elif operation == 'square':
        output = float(first_number) ** 2
    elif operation == 'inverse':
        if first_number=="0":
            output = "ERROR"
        else:
             output = 1 / float(first_number)    
    elif operation == 'negate':
        output = float(first_number) * (-1)
  
    input_entry.delete(0,END)
    input_entry.insert(0,output)
    enable_button()

def enable_button():
    divison_button.config(state=NORMAL)
    multiply_button.config(state=NORMAL)
    add_button.config(state=NORMAL)
    substraction_button.config(state=NORMAL)
    inverse_button.config(state=NORMAL)
    square_button.config(state=NORMAL)
    exponent_button.config(state=NORMAL)
    decimal_button.config(state=NORMAL)

#define layout
input_frame=tkinter.LabelFrame(root,bg=white_green)
input_frame.pack(padx=2,pady=(5,20))
input_button_frame=tkinter.Frame(root,bg=white_green)
input_button_frame.pack(padx=2,pady=5)

#define input layout 
input_entry=tkinter.Entry(input_frame,width=40,borderwidth=5,font=display_font,bg=white_green,justify='right')
input_entry.pack(padx=1,pady=5)

clear_button=tkinter.Button(input_button_frame,text='Clear',font=button_font,command=clear_list,borderwidth=3,bg=dark_green)
quit_button=tkinter.Button(input_button_frame,text='Quit',font=button_font,command=root.destroy,borderwidth=3,bg=dark_green)

inverse_button=tkinter.Button(input_button_frame,text='1/x',font=button_font,bg=light_green,command=lambda:operate('inverse'))
square_button=tkinter.Button(input_button_frame,text='x^2',font=button_font,bg=light_green,command=lambda:operate('square'))
exponent_button=tkinter.Button(input_button_frame,text='x^n',font=button_font,bg=light_green,command=lambda:operate('exponent'))
divison_button=tkinter.Button(input_button_frame,text='/',font=button_font,bg=light_green,command=lambda:operate('divide'))
multiply_button=tkinter.Button(input_button_frame,text='*',font=button_font,bg=light_green,command=lambda:operate('multiply'))
substraction_button=tkinter.Button(input_button_frame,text='-',font=button_font,bg=light_green,command=lambda:operate('substract'))
add_button=tkinter.Button(input_button_frame,text='+',font=button_font,bg=light_green,command=lambda:operate('add'))
equal_button=tkinter.Button(input_button_frame,text='=',font=button_font,bg=dark_green,command =equal)
decimal_button=tkinter.Button(input_button_frame,text='.',font=button_font,bg='black',fg='white',command=lambda:submit_number("."))
negate_button=tkinter.Button(input_button_frame,text='+/-',font=button_font,bg='black',fg='white',command=lambda:operate('negate'))


nine_button=tkinter.Button(input_button_frame,text='9',font=button_font,bg='black',fg='white',command=lambda:submit_number(9))
eight_button=tkinter.Button(input_button_frame,text='8',font=button_font,bg='black',fg='white',command=lambda:submit_number(8))
seven_button=tkinter.Button(input_button_frame,text='7',font=button_font,bg='black',fg='white',command=lambda:submit_number(7))
six_button=tkinter.Button(input_button_frame,text='6',font=button_font,bg='black',fg='white',command=lambda:submit_number(6))
five_button=tkinter.Button(input_button_frame,text='5',font=button_font,bg='black',fg='white',command=lambda:submit_number(5))
four_button=tkinter.Button(input_button_frame,text='4',font=button_font,bg='black',fg='white',command=lambda:submit_number(4))
three_button=tkinter.Button(input_button_frame,text='3',font=button_font,bg='black',fg='white',command=lambda:submit_number(3))
two_button=tkinter.Button(input_button_frame,text='2',font=button_font,bg='black',fg='white',command=lambda:submit_number(2))
one_button=tkinter.Button(input_button_frame,text='1',font=button_font,bg='black',fg='white',command=lambda:submit_number(1))
zero_button=tkinter.Button(input_button_frame,text='0',font=button_font,bg='black',fg='white',command=lambda:submit_number(0))

#first roquit_button.grid(row=0,column=2,columnspan=2,sticky='WE',pady=1)
clear_button.grid(row=0,column=0,columnspan=2,sticky='WE',pady=1)
quit_button.grid(row=0,column=2,columnspan=2,sticky='WE',pady=1)
#second row
inverse_button.grid(row=1,column=0,sticky='WE',pady=1)
square_button.grid(row=1,column=1,sticky='WE',pady=1)
exponent_button.grid(row=1,column=2,sticky='WE',pady=1)
divison_button.grid(row=1,column=3,sticky='WE',pady=1)
#third row
seven_button.grid(row=2,column=0,sticky='WE',pady=1,ipadx=20)
eight_button.grid(row=2,column=1,sticky='WE',pady=1,ipadx=20)
nine_button.grid(row=2,column=2,sticky='WE',pady=1,ipadx=20)
multiply_button.grid(row=2,column=3,sticky='WE',pady=1,ipadx=20)
#fourth row 
four_button.grid(row=3,column=0,sticky='WE',pady=1)
five_button.grid(row=3,column=1,sticky='WE',pady=1)
six_button.grid(row=3,column=2,sticky='WE',pady=1)
substraction_button.grid(row=3,column=3,sticky='WE',pady=1)
#fifth row
one_button.grid(row=4,column=0,sticky='WE',pady=1)
two_button.grid(row=4,column=1,sticky='WE',pady=1)
three_button.grid(row=4,column=2,sticky='WE',pady=1)
add_button.grid(row=4,column=3,sticky='WE',pady=1)
#6th row 
negate_button.grid(row=5,column=0,sticky='WE',pady=1)
zero_button.grid(row=5,column=1,sticky='WE',pady=1)
decimal_button.grid(row=5,column=2,sticky='WE',pady=1)
equal_button.grid(row=5,column=3,sticky='WE',pady=1)


#main loop
root.mainloop()