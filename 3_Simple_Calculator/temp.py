#Author - Ashwinder Singh Garcha
# date - 12 feb 2024 time 8:01pm to 13 feb 2024 9:17pm

import tkinter
from tkinter import END,RIGHT,DISABLED,NORMAL

#main window
root = tkinter.Tk()
root.title("Simple Calculator")
root.iconbitmap("calc.ico")
root.geometry("300x400")
root.resizable(0,0)


#colours and fonts 
root_colour='#CFE795'
button_colour='#F7EF81'
white_green='#edefe0'
root.config(bg=root_colour)
my_font=('Arial',30)
my_Bfont=('Arial',18)


#define function 
def clear():
    output_entry.delete(0,END)


def disable_button():
    
    divide_button.config(state=DISABLED)
    multiply_button.config(state=DISABLED)
    substract_button.config(state=DISABLED)
   
    add_button.config(state=DISABLED)
    inverse_button.config(state=DISABLED)
    derivate_button.config(state=DISABLED)
    sqr_button.config(state=DISABLED)
    sign_button.config(state=DISABLED)
    

def Enable_button():  

    divide_button.config(state=NORMAL)
    multiply_button.config(state=NORMAL)
    substract_button.config(state=NORMAL)
    decimal_button.config(state=NORMAL)
    add_button.config(state=NORMAL)
    inverse_button.config(state=NORMAL)
    derivate_button.config(state=NORMAL)
    sqr_button.config(state=NORMAL)
    sign_button.config(state=NORMAL)


def submit_number(number):
    output_entry.insert(END,number)
    if "." in output_entry.get():
        decimal_button.config(state=DISABLED)
   


def operate(operator):

    global first_number
    global oper 
    oper = operator

    first_number=output_entry.get()
    output_entry.delete(0,END)
    disable_button()
    decimal_button.config(state=NORMAL)


def equal():
    if oper == '+':
        result=float(first_number) + float(output_entry.get())
    elif oper == '-':
        result=float(first_number) - float(output_entry.get())
    elif oper == '^':
        result=float(first_number) ** float(output_entry.get())
    elif oper == '/':
        if output_entry.get()=='0':
            result='ERROR'
        else:
            result=float(first_number) / float(output_entry.get())
    elif oper == '*':  
        if output_entry.get()=='0':
            result='ERROR'
        else:
            result=float(first_number) * float(output_entry.get())

    output_entry.delete(0,END)
    output_entry.insert(0,result)
    Enable_button()   

def inv():
    disable_button()
    decimal_button.config(state=NORMAL)

    result = 1/ float(output_entry.get())
    output_entry.delete(0,END)
    output_entry.insert(0,result)
    Enable_button()  


def sqr():
    disable_button()
    decimal_button.config(state=NORMAL)

    result = float(output_entry.get()) ** 2
    output_entry.delete(0,END)
    output_entry.insert(0,result)
    Enable_button()  


def negate():
    disable_button()
    decimal_button.config(state=NORMAL)

    result = float(output_entry.get()) *(-1)
    output_entry.delete(0,END)
    output_entry.insert(0,result)
    Enable_button()  


#layout
input_frame=tkinter.LabelFrame(root,bg=white_green)
input_frame.pack(padx=2,pady=(5,20))
#output=tkinter.LabelFrame(root,bg='white')
input=tkinter.Frame(root,bg='white')
#output.pack(padx=2,pady=(5,20))
input.pack(padx=2,pady=5)



#output layout
output_entry=tkinter.Entry(input_frame,width=50,borderwidth=5,font=my_font,bg='white',justify='right')
output_entry.pack(padx=1,pady=5)

#input layout 
clear_button=tkinter.Button(input,text='Clear',font=my_Bfont,bg=button_colour,borderwidth=3,command=clear)
quit_button=tkinter.Button(input,text='Quit',font=my_Bfont,bg=button_colour,borderwidth=3,command=root.destroy)

inverse_button=tkinter.Button(input,text='1/x',font=my_Bfont,bg=button_colour,command=inv)
derivate_button=tkinter.Button(input,text='x^n',font=my_Bfont,bg=button_colour,command=lambda:operate('^'))
sqr_button=tkinter.Button(input,text='x^2',font=my_Bfont,bg=button_colour,command=sqr)
divide_button=tkinter.Button(input,text='/',font=my_Bfont,bg=button_colour,command=lambda:operate('/'))

seven_button=tkinter.Button(input,text='7',font=my_Bfont,bg='black',fg="white",command=lambda:submit_number(7))
eight_button=tkinter.Button(input,text='8',font=my_Bfont,bg='black',fg="white",command=lambda:submit_number(8))
nine_button=tkinter.Button(input,text='9',font=my_Bfont,bg='black',fg="white",command=lambda:submit_number(9))
multiply_button=tkinter.Button(input,text='*',font=my_Bfont,bg=button_colour,command=lambda:operate('*'))

four_button=tkinter.Button(input,text='4',font=my_Bfont,bg='black',fg="white",command=lambda :submit_number(4))
five_button=tkinter.Button(input,text='5',font=my_Bfont,bg='black',fg="white",command=lambda:submit_number(5))
six_button=tkinter.Button(input,text='6',font=my_Bfont,bg='black',fg="white",command=lambda :submit_number(6))
substract_button=tkinter.Button(input,text='-',font=my_Bfont,bg=button_colour,command=lambda:operate('-'))

one_button=tkinter.Button(input,text='1',font=my_Bfont,bg='black',fg="white",command=lambda :submit_number(1))
two_button=tkinter.Button(input,text='2',font=my_Bfont,bg='black',fg="white",command=lambda :submit_number(2))
three_button=tkinter.Button(input,text='3',font=my_Bfont,bg='black',fg="white",command=lambda :submit_number(3))
add_button=tkinter.Button(input,text='+',font=my_Bfont,bg=button_colour,command=lambda:operate('+'))

sign_button=tkinter.Button(input,text='+/-',font=my_Bfont,bg='black',fg="white",command=negate)
zero_button=tkinter.Button(input,text='0',font=my_Bfont,bg='black',fg="white",command=lambda:submit_number(0))
decimal_button=tkinter.Button(input,text='.',font=my_Bfont,bg='black',fg="white",command=lambda:submit_number("."))
equal_button=tkinter.Button(input,text='=',font=my_Bfont,bg=button_colour,command=equal)


#0 row
clear_button.grid(row=0,column=0,columnspan=2,sticky='WE',pady=1)
quit_button.grid(row=0,column=2,columnspan=2,sticky='WE',pady=1)

inverse_button.grid(row=1,column=0,sticky='WE',pady=1)
derivate_button.grid(row=1,column=1,sticky='WE',pady=1)
sqr_button.grid(row=1,column=2,sticky='WE',pady=1)
divide_button.grid(row=1,column=3,sticky='WE',pady=1)
#2row
seven_button.grid(row=2,column=0,sticky='WE',pady=1,ipadx=20)
eight_button.grid(row=2,column=1,sticky='WE',pady=1,ipadx=20)
nine_button.grid(row=2,column=2,sticky='WE',pady=1,ipadx=20)
multiply_button.grid(row=2,column=3,sticky='WE',pady=1,ipadx=20)
#3row
four_button.grid(row=3,column=0,sticky='WE',pady=1)
five_button.grid(row=3,column=1,sticky='WE',pady=1)
six_button.grid(row=3,column=2,sticky='WE',pady=1)
substract_button.grid(row=3,column=3,sticky='WE',pady=1)
#4row
one_button.grid(row=4,column=0,sticky='WE',pady=1)
two_button.grid(row=4,column=1,sticky='WE',pady=1)
three_button.grid(row=4,column=2,sticky='WE',pady=1)
add_button.grid(row=4,column=3,sticky='WE',pady=1)
#5row
sign_button.grid(row=5,column=0,sticky='WE',pady=1)
zero_button.grid(row=5,column=1,sticky='WE',pady=1)
decimal_button.grid(row=5,column=2,sticky='WE',pady=1)
equal_button.grid(row=5,column=3,sticky='WE',pady=1)


#main loop
root.mainloop()


