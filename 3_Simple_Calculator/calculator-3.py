#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH  date 15 feb time 9:40 PM 
# colour picker  https://imagecolorpicker.com/
# icon website https://www.iconarchive.com/
# colour themes  https://coolors.co/

import tkinter
from tkinter import RIGHT,END,DISABLED,NORMAL

#window
root=tkinter.Tk()
root.title('Simple Calculator')
root.geometry("300x400")
root.iconbitmap('calc.ico')
root.resizable(0,0)

#font and colour 
input_font =('Arial',32)
button_font=('Arial',18)
white_green='#acc054'
light_green='#93af22'
dark_green='#556c11'

#define functions
#0disableD all button except numbers
def disable_buttons():
    inverse_button.config(state=DISABLED)
    square_button.config(state=DISABLED)
    derivative_button.config(state=DISABLED)
    divide_button.config(state=DISABLED)
    addition_button.config(state=DISABLED)
    substraction_button.config(state=DISABLED)
    multiplication_button.config(state=DISABLED)
    negate_button.config(state=DISABLED)
   # decimal_button.config(state=DISABLED)

#0Enable all button except numbers
def Enable_buttons():
    inverse_button.config(state=NORMAL)
    square_button.config(state=NORMAL)
    derivative_button.config(state=NORMAL)
    divide_button.config(state=NORMAL)
    addition_button.config(state=NORMAL)
    substraction_button.config(state=NORMAL)
    multiplication_button.config(state=NORMAL)
    negate_button.config(state=NORMAL)
    #decimal_button.config(state=NORMAL)

#1close app
def close_app():
    root.destroy()


#2clear input 
def clear_screen():
    input_entry.delete(0,END)
    Enable_buttons()

#3take input when button pressed 7,8,9,0,1,2,3,4,5,6,. and disbale decimal if it is precessed
def submit(number):
    #print(number)
   
    input_entry.insert(END,number)
    if "." in input_entry.get():
         decimal_button.config(state=DISABLED)

#4take operators and first number , if operator pressed disableD all operators and enable decimal for second number 
def operator(operations):
   
    global first_number
    global operation
    operation=operations 

    #take first number and clear input scren
    first_number=input_entry.get()
    #print(first_number)
    #print(operation)
    input_entry.delete(0,END)

    #Enable decimal
    decimal_button.config(state=NORMAL)
    #disable all operators except decimal
    disable_buttons()

#5equal button to perform calculations input is first number in function 4 , operator also used from 4th function globaly , display result , reenable all buttons
#5 function do + - * / x^n
def equal():

    if operation == '+':
        result = float(first_number) + float(input_entry.get())
        #print(result)
    elif operation == '-':
        result = float(first_number) - float(input_entry.get())
    elif operation == '*':
        if float(input_entry.get())==0 :
               result = "ERROR" 
        else:
          result = float(first_number) * float(input_entry.get())  
    elif operation == '/':
        if float(input_entry.get())==0 :
            result = "ERROR"         
        else:
          result = float(first_number) / float(input_entry.get())
    elif operation == 'x^n':
        result = float(first_number) ** float(input_entry.get())   
    
    
    #clear last entry before print result 
    input_entry.delete(0,END)
    input_entry.insert(END,result)
    #enable operators
    Enable_buttons()

#6 function do singal button operation 1/x
def inver():
    disable_buttons()
    result = 1/float(input_entry.get())
    input_entry.delete(0,END)
    input_entry.insert(END,result)
     #enable operators
    Enable_buttons()

#7 function do singal button operation x^2
def squr():
    disable_buttons()
    result = float(input_entry.get()) ** 2
    input_entry.delete(0,END)
    input_entry.insert(END,result)
     #enable operators
    Enable_buttons()

#7 function do singal button operation +/-
def neg():
    disable_buttons()
    result = float(input_entry.get()) *(-1)
    input_entry.delete(0,END)
    input_entry.insert(END,result)
     #enable operators
    Enable_buttons()

#layout pack
input_frame = tkinter.LabelFrame(root,bg='white')
input_frame.pack(padx=1,pady=(0,5))
input_button_frame=tkinter.Frame(root,bg='white')
input_button_frame.pack(padx=1,pady=(20,0))

#input Frame pack
input_entry=tkinter.Entry(input_frame,font=input_font,borderwidth=5,justify=RIGHT,width=50)
input_entry.pack(padx=2,pady=5)

#input buttonFrame 
clear_button=tkinter.Button(input_button_frame,text='Clear',borderwidth=2,font=button_font,bg=light_green,command=clear_screen)
quit_button=tkinter.Button(input_button_frame,text='Quit',borderwidth=2,font=button_font,bg=light_green,command=close_app)

addition_button=tkinter.Button(input_button_frame,text='+',borderwidth=2,font=button_font,bg=white_green,command=lambda:operator("+"))
substraction_button=tkinter.Button(input_button_frame,text='-',borderwidth=2,font=button_font,bg=white_green,command=lambda:operator("-"))
multiplication_button=tkinter.Button(input_button_frame,text='*',borderwidth=2,font=button_font,bg=white_green,command=lambda:operator("*"))
divide_button=tkinter.Button(input_button_frame,text=' / ',borderwidth=2,font=button_font,bg=white_green,command=lambda:operator("/"))
derivative_button=tkinter.Button(input_button_frame,text='x^n',borderwidth=2,font=button_font,bg=white_green,command=lambda:operator("x^n"))

inverse_button=tkinter.Button(input_button_frame,text='1/x',borderwidth=2,font=button_font,bg=white_green,command=inver)
square_button=tkinter.Button(input_button_frame,text='x^2',borderwidth=2,font=button_font,bg=white_green,command=squr)
negate_button=tkinter.Button(input_button_frame,text='+/-',borderwidth=2,font=button_font,bg='black',fg="white",command=neg)

decimal_button=tkinter.Button(input_button_frame,text='.',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit("."))
zero_button=tkinter.Button(input_button_frame,text='0',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit('0'))
One_button=tkinter.Button(input_button_frame,text='1',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit('1'))
two_button=tkinter.Button(input_button_frame,text='2',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit('2'))
three_button=tkinter.Button(input_button_frame,text='3',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit('3'))
Four_button=tkinter.Button(input_button_frame,text='4',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit('4'))
five_button=tkinter.Button(input_button_frame,text='5',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit('5'))
Six_button=tkinter.Button(input_button_frame,text='6',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit('6'))
seven_button=tkinter.Button(input_button_frame,text='7',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit('7'))
eight_button=tkinter.Button(input_button_frame,text='8',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit('8'))
Nine_button=tkinter.Button(input_button_frame,text='9',borderwidth=2,font=button_font,bg='black',fg="white",command=lambda:submit('9'))

equal_button=tkinter.Button(input_button_frame,text='=',borderwidth=2,font=button_font,bg=white_green,command=equal)

#row0
clear_button.grid(row=0,column=0,columnspan=2,pady=1,sticky='WE')
quit_button.grid(row=0,column=2,columnspan=2,pady=1,sticky='WE')
#row1
inverse_button.grid(row=1,column=0,pady=1,ipadx=10)
square_button.grid(row=1,column=1,pady=1,ipadx=10)
derivative_button.grid(row=1,column=2,pady=1,ipadx=10)
divide_button.grid(row=1,column=3,pady=1,ipadx=10)
#row2
seven_button.grid(row=2,column=0,pady=1,sticky='WE')
eight_button.grid(row=2,column=1,pady=1,sticky='WE')
Nine_button.grid(row=2,column=2,pady=1,sticky='WE')
multiplication_button.grid(row=2,column=3,pady=1,sticky='WE')
#row3
Four_button.grid(row=3,column=0,pady=1,sticky='WE')
five_button.grid(row=3,column=1,pady=1,sticky='WE')
Six_button.grid(row=3,column=2,pady=1,sticky='WE')
substraction_button.grid(row=3,column=3,pady=1,sticky='WE')
#row4
One_button.grid(row=4,column=0,pady=1,sticky='WE')
two_button.grid(row=4,column=1,pady=1,sticky='WE')
three_button.grid(row=4,column=2,pady=1,sticky='WE')
addition_button.grid(row=4,column=3,pady=1,sticky='WE')
#row5
negate_button.grid(row=5,column=0,pady=1,sticky='WE')
zero_button.grid(row=5,column=1,pady=1,sticky='WE')
decimal_button.grid(row=5,column=2,pady=1,sticky='WE')
equal_button.grid(row=5,column=3,pady=1,sticky='WE')

#main loop
root.mainloop()