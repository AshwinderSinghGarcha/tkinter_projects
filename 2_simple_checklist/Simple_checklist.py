#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 
# colour picker  https://imagecolorpicker.com/
# icon website https://www.iconarchive.com/
# colour themes  https://coolors.co/


import tkinter
from tkinter import END,ANCHOR

#window
root=tkinter.Tk()
root.title('Garcha Simple Checklist')
root.iconbitmap('check.ico')
root.geometry('400x400')
root.resizable(0,0)


#define function 
def add():
    output_listbox.insert(END,input_entry.get())      #index end becz we want to add after last not to first 
    input_entry.delete(0,END)                        #to clear input bar from index 0 to last 


def Remove_item():
    output_listbox.delete(ANCHOR)


def Clear_list():
    output_listbox.delete(0,END)


def Save_list():
    with open('file.txt','w') as f:
        my_tuple=output_listbox.get(0,END)
        for item in my_tuple:
            if item.endswith('\n'):
                f.write(item)
            else:
                f.write(item +'\n')



def open_list():
    '''open list if there is one '''
    try:
        with open('file.txt','r') as f:
            for line in f:
                output_listbox.insert(END,line)
    except:
        return


#define fonts colour
button_colour= '#e2cff4'
root_colour='#6c1cbc'
My_font=('Times New Roman',12)
root.config(bg=root_colour)

#define layout 
input_frame  =tkinter.Frame(root,bg=root_colour)
output_frame =tkinter.Frame(root,bg=root_colour)
button_frame =tkinter.Frame(root,bg=root_colour)
input_frame.pack()
output_frame.pack()
button_frame.pack()

#input layout
input_entry=tkinter.Entry(input_frame,width=35,borderwidth=3,font=My_font)
add_item=tkinter.Button(input_frame,text='Add item',bg=button_colour,font=My_font,borderwidth=2,command=add)
input_entry.grid(row=0,column=0,padx=5,pady=5)
add_item.grid(row=0,column=1,padx=5,pady=5,ipadx=5)


#output layout 
my_scrolbar=tkinter.Scrollbar(output_frame)
output_listbox=tkinter.Listbox(output_frame,width=45,height=15,font=My_font,yscrollcommand=my_scrolbar.set)
#link scrollbar to outputlist
my_scrolbar.config(command=output_listbox.yview)
output_listbox.grid(row=0,column=0)
my_scrolbar.grid(row=0,column=1,sticky='NS')


#button layout
remove_button=tkinter.Button(button_frame,text='Remove Item',bg=button_colour,font=My_font,borderwidth=2,command=Remove_item)
Clear_button=tkinter.Button(button_frame,text='Clear List',bg=button_colour,font=My_font,borderwidth=2,command=Clear_list)
Save_button=tkinter.Button(button_frame,text='Save List',bg=button_colour,font=My_font,borderwidth=2,command=Save_list)
Quit_button=tkinter.Button(button_frame,text='Quit',bg=button_colour,font=My_font,borderwidth=2,command=root.destroy)

remove_button.grid(row=0,column=0,padx=2,pady=10)
Clear_button.grid(row=0,column=1,padx=2,pady=10,ipadx=10)
Save_button.grid(row=0,column=2,padx=2,pady=10,ipadx=10)
Quit_button.grid(row=0,column=3,padx=2,pady=10,ipadx=25)



#open old list if avial
open_list()
#main loop
root.mainloop()