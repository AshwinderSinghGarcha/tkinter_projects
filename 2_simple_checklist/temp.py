#author - ASHWINDER SINGH GARCHA 
#HAR MAIDAN FATEH 
# colour picker  https://imagecolorpicker.com/
# icon website https://www.iconarchive.com/
# colour themes  https://coolors.co/


import tkinter
from tkinter import END,ANCHOR

#define window 
root=tkinter.Tk()
root.title('Simple Checklist')
root.iconbitmap('bison.ico')
root.geometry('400x400')
root.resizable(0,0)

#define function 
#add item
def add():
    output.insert(END,entry_box.get())
    entry_box.delete(0,END)
    
#remove item
def remove():
    output.delete(ANCHOR)

#clear list
def Clear():
    output.delete(0,END)

#save list
def Save():
    with open('list.txt','w') as f:
        my_tuple=output.get(0,END)
        print(my_tuple)
        for item in my_tuple:
            if item.endswith('\n'):
                f.write(item)
            else:
                f.write(item + '\n')

#open list

def open_list():
    try:
        with open('list.txt','r') as f:
            for line in f :
                output.insert(END,line)
    except:
        return
#define font and colour
root_colour='#6A2E35'
button_colour='#BE8A60'
my_font=('Times new roman',12)
root.config(bg=root_colour)


#define FRAMES
input_frame=tkinter.Frame(root,bg=root_colour)
output_frame=tkinter.Frame(root,bg=root_colour)
button_frame=tkinter.Frame(root,bg=root_colour)
input_frame.pack()
output_frame.pack()
button_frame.pack()

#define input layout 
entry_box=tkinter.Entry(input_frame,width=35,borderwidth=3,font=my_font)
add_button=tkinter.Button(input_frame,text='Add item',bg=button_colour,font=my_font,borderwidth=2,command=add)
entry_box.grid(row=0,column=0,padx=5,pady=5)
add_button.grid(row=0,column=1,padx=5,pady=5,ipadx=5)

#define output layout

my_scrollbar=tkinter.Scrollbar(output_frame)
output=tkinter.Listbox(output_frame,width=45,height=15,font=my_font,yscrollcommand=my_scrollbar.set)
output.grid(row=0,column=0)
my_scrollbar.grid(row=0,column=1,sticky='NS')
my_scrollbar.config(command=output.yview)

#define button layout
remove_button=tkinter.Button(button_frame,text='Remove Item',bg=button_colour,font=my_font,borderwidth=2,command=remove)
Clear_button=tkinter.Button(button_frame,text='Clear List',bg=button_colour,font=my_font,borderwidth=2,command=Clear)
Save_button=tkinter.Button(button_frame,text='Save List',bg=button_colour,font=my_font,borderwidth=2,command=Save)
Quit_button=tkinter.Button(button_frame,text='Quit',bg=button_colour,font=my_font,borderwidth=2,command=root.destroy)

remove_button.grid(row=0,column=0,padx=2,pady=10)
Clear_button.grid(row=0,column=1,padx=2,pady=10,ipadx=10)
Save_button.grid(row=0,column=2,padx=2,pady=10,ipadx=10)
Quit_button.grid(row=0,column=3,padx=2,pady=10,ipadx=25)

open_list()
#main loop
root.mainloop()
